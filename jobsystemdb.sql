-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2022 at 11:38 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobsystemdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_mail_campaign_detail`
--

CREATE TABLE `admin_mail_campaign_detail` (
  `id` int(11) NOT NULL,
  `admin_template_id` int(11) DEFAULT NULL,
  `admin_mail_config_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `receipt_status` tinyint(1) DEFAULT 0,
  `receipt_time` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_mail_campaign_detail`
--

INSERT INTO `admin_mail_campaign_detail` (`id`, `admin_template_id`, `admin_mail_config_id`, `user_id`, `user_email`, `receipt_status`, `receipt_time`, `created_at`, `created_by`) VALUES
(22, 8, 1, 2, 'vinhhofb@gmail.com', 1, 1651823195, 1651823192, 1),
(23, 8, 1, 3, 'nguyenvanb@gmail.com', 1, 1651823198, 1651823192, 1),
(24, 8, 1, 4, 'vinhhovinhho@gmail.com', 1, 1651823201, 1651823192, 1),
(25, 8, 1, 5, 'vinh44344@donga.edu.vn', 1, 1651823204, 1651823192, 1),
(26, 7, 1, 4, 'vinhhovinhho@gmail.com', 1, 1651823638, 1651823635, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_mail_config`
--

CREATE TABLE `admin_mail_config` (
  `id` int(11) NOT NULL,
  `mail_drive` varchar(10) DEFAULT NULL,
  `mail_host` char(50) DEFAULT NULL,
  `mail_port` int(11) DEFAULT NULL,
  `mail_username` varchar(255) DEFAULT NULL,
  `mail_password` varchar(255) DEFAULT NULL,
  `mail_encryption` char(50) DEFAULT NULL,
  `total_send` int(11) DEFAULT 0,
  `is_deleted` int(11) DEFAULT 0,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_mail_config`
--

INSERT INTO `admin_mail_config` (`id`, `mail_drive`, `mail_host`, `mail_port`, `mail_username`, `mail_password`, `mail_encryption`, `total_send`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, NULL, 'smtp.gmail.com', 587, 'yatchinvest@gmail.com', 'gbgdncosyqbwxrwt', 'tls', 0, 0, 1651818848, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_mail_template`
--

CREATE TABLE `admin_mail_template` (
  `id` int(11) NOT NULL,
  `template_title` varchar(250) DEFAULT NULL,
  `template_content` text DEFAULT NULL,
  `total_send` int(11) DEFAULT 0,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `show_order` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_mail_template`
--

INSERT INTO `admin_mail_template` (`id`, `template_title`, `template_content`, `total_send`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`, `show_order`) VALUES
(1, 'Thông báo công việc', '<p>Bạn vừa được giao một c&ocirc;ng việc mới, Truy cập <a href=\"https://jobmanagesystem.com/quan-ly-cong-viec\">https://jobmanagesystem.com/quan-ly-cong-viec</a> để biết th&ecirc;m th&ocirc;ng tin chi tiết</p>', 0, 0, NULL, NULL, 1651816875, 1, 0),
(2, 'Thay đổi nội dung công việc', '<p>Nội dung c&ocirc;ng việc của bạn đ&atilde; bị thay đổi, vui l&ograve;ng truy cập <a href=\"/quan-ly-cong-viec\">http://jobmanagesystem.com/quan-ly-cong-viec</a> để xem chi tiết</p>', 0, 0, 1651809950, NULL, 1651817773, 1, 0),
(7, 'Xin chào', '<p>Xin ch&agrave;o th&agrave;nh vi&ecirc;n mới</p>', 0, 1, 1651818933, NULL, 1651819095, 1, 0),
(8, 'Lịch nghỉ tết', '<p>Lịch nghỉ tết của c&ocirc;ng ty từ ng&agrave;y ...</p>\r\n<p>&nbsp;</p>', 0, 0, 1651818959, NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bang_tinh_luong`
--

CREATE TABLE `bang_tinh_luong` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `thang` int(11) DEFAULT NULL,
  `gio_cong` int(11) DEFAULT NULL,
  `ngay_tao` int(11) DEFAULT NULL,
  `nguoi_tao` int(11) DEFAULT NULL,
  `ngay_sua` int(11) DEFAULT NULL,
  `nguoi_sua` int(11) DEFAULT NULL,
  `xoa` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bang_tinh_luong`
--

INSERT INTO `bang_tinh_luong` (`id`, `user_id`, `thang`, `gio_cong`, `ngay_tao`, `nguoi_tao`, `ngay_sua`, `nguoi_sua`, `xoa`) VALUES
(1, 2, 5, 66, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `chuc_vu`
--

CREATE TABLE `chuc_vu` (
  `id` int(11) NOT NULL,
  `ten_chuc_vu` varchar(255) DEFAULT NULL,
  `mo_ta` varchar(255) DEFAULT NULL,
  `luong_ngay` int(11) DEFAULT NULL,
  `nguoi_tao` int(11) DEFAULT NULL,
  `ngay_tao` int(11) DEFAULT NULL,
  `ngay_sua` int(11) DEFAULT NULL,
  `nguoi_sua` int(11) DEFAULT NULL,
  `xoa` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chuc_vu`
--

INSERT INTO `chuc_vu` (`id`, `ten_chuc_vu`, `mo_ta`, `luong_ngay`, `nguoi_tao`, `ngay_tao`, `ngay_sua`, `nguoi_sua`, `xoa`) VALUES
(1, 'Nhân viên', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 'Trưởng phòng', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, 'Giám đốc', NULL, NULL, 1, 1651551254, NULL, NULL, 0),
(4, 'a', 'a', NULL, 1, 1651551130, 1651551132, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chuyen_mon`
--

CREATE TABLE `chuyen_mon` (
  `id` int(11) NOT NULL,
  `ten_chuyen_mon` varchar(255) DEFAULT NULL,
  `mo_ta` text DEFAULT NULL,
  `nguoi_tao` int(11) DEFAULT NULL,
  `ngay_tao` int(11) DEFAULT NULL,
  `nguoi_sua` int(11) DEFAULT NULL,
  `ngay_sua` int(11) DEFAULT NULL,
  `xoa` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chuyen_mon`
--

INSERT INTO `chuyen_mon` (`id`, `ten_chuyen_mon`, `mo_ta`, `nguoi_tao`, `ngay_tao`, `nguoi_sua`, `ngay_sua`, `xoa`) VALUES
(1, 'Maketing', NULL, NULL, NULL, NULL, NULL, 0),
(2, 'Lập trình', NULL, 1, 1651552227, NULL, NULL, 0),
(3, 'a', 'a', 1, 1651552232, 1, 1651552234, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ch_favorites`
--

CREATE TABLE `ch_favorites` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `favorite_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ch_messages`
--

CREATE TABLE `ch_messages` (
  `id` bigint(20) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_id` bigint(20) NOT NULL,
  `to_id` bigint(20) NOT NULL,
  `body` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ch_messages`
--

INSERT INTO `ch_messages` (`id`, `type`, `from_id`, `to_id`, `body`, `attachment`, `seen`, `created_at`, `updated_at`) VALUES
(1683148461, 'user', 51, 45, 'xin ch&agrave;o', NULL, 0, '2022-03-25 23:59:40', '2022-03-25 23:59:40'),
(1744624539, 'user', 51, 50, 'ok', NULL, 1, '2022-03-26 00:35:57', '2022-03-26 00:35:57'),
(1825266546, 'user', 51, 50, 'xin ch&agrave;o', NULL, 1, '2022-03-26 00:30:38', '2022-03-26 00:30:38'),
(1839869590, 'user', 51, 45, 'alo', NULL, 0, '2022-03-26 00:00:21', '2022-03-26 00:00:21'),
(1853190854, 'user', 22, 23, 'alo', NULL, 1, '2022-04-16 00:23:50', '2022-04-16 00:23:51'),
(1968774321, 'user', 23, 22, 'ok', NULL, 1, '2022-04-16 00:23:03', '2022-04-16 00:23:04'),
(2003480658, 'user', 50, 51, 'ok', NULL, 1, '2022-03-25 23:27:22', '2022-03-25 23:27:23'),
(2037952703, 'user', 51, 45, 'ola', NULL, 0, '2022-03-25 23:59:14', '2022-03-25 23:59:14'),
(2039709704, 'user', 22, 23, 'ok', NULL, 1, '2022-04-16 00:23:23', '2022-04-16 00:23:24'),
(2192247998, 'user', 22, 23, 'ok', NULL, 1, '2022-04-16 00:24:48', '2022-04-16 00:24:48'),
(2198412295, 'user', 51, 50, 'alo', NULL, 1, '2022-03-25 23:27:09', '2022-03-25 23:27:14'),
(2233956062, 'user', 51, 45, '', '{\"new_name\":\"14c5d18c-95dd-40a6-8c87-18af6e370242.jpg\",\"old_name\":\"2.jpg\"}', 0, '2022-03-26 00:01:23', '2022-03-26 00:01:23'),
(2317585526, 'user', 51, 45, 'ok', NULL, 0, '2022-03-25 23:59:21', '2022-03-25 23:59:21'),
(2326314486, 'user', 22, 23, 'alo', NULL, 1, '2022-04-16 00:21:51', '2022-04-16 00:23:01'),
(2337647559, 'user', 51, 50, 'd', NULL, 1, '2022-03-26 00:04:46', '2022-03-26 00:04:47'),
(2387483355, 'user', 51, 44, 'ola', NULL, 0, '2022-03-25 23:38:55', '2022-03-25 23:38:55'),
(2428343823, 'user', 53, 43, 'alo', NULL, 0, '2022-03-25 22:56:38', '2022-03-25 22:56:38'),
(2495660778, 'user', 51, 43, 'alo', NULL, 0, '2022-03-25 23:22:01', '2022-03-25 23:22:01'),
(2523541020, 'user', 22, 23, '123', NULL, 1, '2022-04-16 00:23:38', '2022-04-16 00:23:39'),
(2541163544, 'user', 51, 45, 'alo', NULL, 0, '2022-03-25 23:58:50', '2022-03-25 23:58:50'),
(2561103900, 'user', 51, 50, 'alo', NULL, 1, '2022-03-26 00:02:38', '2022-03-26 00:02:44'),
(2581421728, 'user', 51, 44, 'alo', NULL, 0, '2022-03-25 23:34:40', '2022-03-25 23:34:40');

-- --------------------------------------------------------

--
-- Table structure for table `cong_viec`
--

CREATE TABLE `cong_viec` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ten_cong_viec` varchar(255) DEFAULT NULL,
  `mo_ta` text DEFAULT NULL,
  `tu_ngay` int(11) DEFAULT NULL,
  `den_ngay` int(11) DEFAULT NULL,
  `trang_thai` int(11) DEFAULT 0,
  `ngay_tao` int(11) DEFAULT NULL,
  `nguoi_tao` int(11) DEFAULT NULL,
  `ngay_sua` int(11) DEFAULT NULL,
  `nguoi_sua` int(11) DEFAULT NULL,
  `xoa` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cong_viec`
--

INSERT INTO `cong_viec` (`id`, `user_id`, `ten_cong_viec`, `mo_ta`, `tu_ngay`, `den_ngay`, `trang_thai`, `ngay_tao`, `nguoi_tao`, `ngay_sua`, `nguoi_sua`, `xoa`) VALUES
(1, 2, 'Lên chiến dịch quảng cáo', 'Lên chiến dịch quảng cáo', 1651806819, 1651806825, 0, NULL, NULL, 1651817995, 1, 0),
(2, 3, 'Thiết kế sản phẩm gửi cho khách hàng', 'Thiết kế sản phẩm gửi cho khách hàng', 1651806819, 1651806825, 0, NULL, NULL, NULL, NULL, 0),
(3, 2, 'fd', 'df', 1651795200, 1652486400, 0, 1651816794, 1, 1651817146, 1, 1),
(4, 4, 'Lên kế hoạch bán hàng tháng 6', 'Lên kế hoạch bán hàng tháng 6', 1651795200, 1652486400, 0, 1651816968, 1, 1651817124, 1, 1),
(5, 4, 'Lên kế hoạch bán hàng tháng 6', 'Lên kế hoạch bán hàng tháng 6', 1651795200, 1652486400, 0, 1651817031, 1, NULL, NULL, 0),
(6, 5, 'sửa2', 'sửa', 1651881600, 1653091200, 0, 1651817207, 1, 1651817923, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `failed_jobs`
--

INSERT INTO `failed_jobs` (`id`, `uuid`, `connection`, `queue`, `payload`, `exception`, `failed_at`) VALUES
(1, '83ffdc28-d587-4b10-835f-4ed32fe16b80', 'database', 'default', '{\"uuid\":\"83ffdc28-d587-4b10-835f-4ed32fe16b80\",\"displayName\":\"App\\\\Jobs\\\\SendEmailCampaignNow\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmailCampaignNow\",\"command\":\"O:29:\\\"App\\\\Jobs\\\\SendEmailCampaignNow\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";i:1;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'Error: Class \'App\\Jobs\\BD\' not found in C:\\xampp\\htdocs\\JobManageSystem\\app\\Jobs\\SendEmailCampaignNow.php:39\nStack trace:\n#0 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(36): App\\Jobs\\SendEmailCampaignNow->handle()\n#1 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#2 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#3 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#4 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(653): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#5 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(128): Illuminate\\Container\\Container->call(Array)\n#6 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#7 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#8 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(132): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#9 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(120): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(App\\Jobs\\SendEmailCampaignNow), false)\n#10 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#11 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#12 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#13 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(70): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(App\\Jobs\\SendEmailCampaignNow))\n#14 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Jobs\\Job.php(98): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#15 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(428): Illuminate\\Queue\\Jobs\\Job->fire()\n#16 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(378): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#17 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(172): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#18 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(117): Illuminate\\Queue\\Worker->daemon(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#19 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(101): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#20 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(36): Illuminate\\Queue\\Console\\WorkCommand->handle()\n#21 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#22 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#23 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#24 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(653): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#25 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(136): Illuminate\\Container\\Container->call(Array)\n#26 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Command\\Command.php(298): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#27 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#28 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Application.php(1015): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#29 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Application.php(299): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#30 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Application.php(171): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#31 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(94): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(129): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 C:\\xampp\\htdocs\\JobManageSystem\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 {main}', '2022-05-06 00:23:00'),
(2, 'b40c0fb3-b8fa-420f-ae6b-e45b49b7321e', 'database', 'default', '{\"uuid\":\"b40c0fb3-b8fa-420f-ae6b-e45b49b7321e\",\"displayName\":\"App\\\\Jobs\\\\SendEmailCampaignNow\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmailCampaignNow\",\"command\":\"O:29:\\\"App\\\\Jobs\\\\SendEmailCampaignNow\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";i:1;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'Error: Class \'App\\Jobs\\BD\' not found in C:\\xampp\\htdocs\\JobManageSystem\\app\\Jobs\\SendEmailCampaignNow.php:39\nStack trace:\n#0 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(36): App\\Jobs\\SendEmailCampaignNow->handle()\n#1 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#2 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#3 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#4 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(653): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#5 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(128): Illuminate\\Container\\Container->call(Array)\n#6 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#7 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#8 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(132): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#9 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(120): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(App\\Jobs\\SendEmailCampaignNow), false)\n#10 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#11 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#12 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#13 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(70): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(App\\Jobs\\SendEmailCampaignNow))\n#14 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Jobs\\Job.php(98): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#15 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(428): Illuminate\\Queue\\Jobs\\Job->fire()\n#16 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(378): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#17 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(172): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#18 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(117): Illuminate\\Queue\\Worker->daemon(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#19 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(101): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#20 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(36): Illuminate\\Queue\\Console\\WorkCommand->handle()\n#21 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#22 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#23 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#24 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(653): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#25 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(136): Illuminate\\Container\\Container->call(Array)\n#26 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Command\\Command.php(298): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#27 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#28 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Application.php(1015): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#29 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Application.php(299): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#30 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Application.php(171): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#31 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(94): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(129): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 C:\\xampp\\htdocs\\JobManageSystem\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 {main}', '2022-05-06 00:23:33'),
(3, '099f6371-0ef6-46b2-9961-bfc79dd54e3c', 'database', 'default', '{\"uuid\":\"099f6371-0ef6-46b2-9961-bfc79dd54e3c\",\"displayName\":\"App\\\\Jobs\\\\SendEmailCampaignNow\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmailCampaignNow\",\"command\":\"O:29:\\\"App\\\\Jobs\\\\SendEmailCampaignNow\\\":11:{s:5:\\\"\\u0000*\\u0000id\\\";i:1;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'Error: Class \'App\\Jobs\\BD\' not found in C:\\xampp\\htdocs\\JobManageSystem\\app\\Jobs\\SendEmailCampaignNow.php:39\nStack trace:\n#0 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(36): App\\Jobs\\SendEmailCampaignNow->handle()\n#1 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#2 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#3 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#4 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(653): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#5 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(128): Illuminate\\Container\\Container->call(Array)\n#6 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#7 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#8 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(132): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#9 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(120): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(App\\Jobs\\SendEmailCampaignNow), false)\n#10 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#11 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\SendEmailCampaignNow))\n#12 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#13 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(70): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(App\\Jobs\\SendEmailCampaignNow))\n#14 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Jobs\\Job.php(98): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#15 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(428): Illuminate\\Queue\\Jobs\\Job->fire()\n#16 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(378): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#17 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(172): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#18 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(117): Illuminate\\Queue\\Worker->daemon(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#19 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(101): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#20 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(36): Illuminate\\Queue\\Console\\WorkCommand->handle()\n#21 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#22 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#23 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#24 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(653): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#25 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(136): Illuminate\\Container\\Container->call(Array)\n#26 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Command\\Command.php(298): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#27 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#28 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Application.php(1015): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#29 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Application.php(299): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#30 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\symfony\\console\\Application.php(171): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#31 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(94): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 C:\\xampp\\htdocs\\JobManageSystem\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(129): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 C:\\xampp\\htdocs\\JobManageSystem\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 {main}', '2022-05-06 00:26:10');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `khen_thuong_ky_luat`
--

CREATE TABLE `khen_thuong_ky_luat` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `mo_ta` text DEFAULT NULL,
  `gia_tri` int(11) DEFAULT NULL,
  `loai` int(11) DEFAULT NULL,
  `ngay_tao` int(11) DEFAULT NULL,
  `nguoi_tao` int(11) DEFAULT NULL,
  `ngay_sua` int(11) DEFAULT NULL,
  `nguoi_sua` int(11) DEFAULT NULL,
  `xoa` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `khen_thuong_ky_luat`
--

INSERT INTO `khen_thuong_ky_luat` (`id`, `user_id`, `mo_ta`, `gia_tri`, `loai`, `ngay_tao`, `nguoi_tao`, `ngay_sua`, `nguoi_sua`, `xoa`) VALUES
(1, 2, 'Khen thưởng abc', 500000, 0, NULL, NULL, 1651804937, 1, 1),
(2, 3, 'Làm việc năng suất', 700000, 0, NULL, NULL, 1651804506, 1, 1),
(3, 3, 'Sử dụng điện thoại trong giờ làm việc', 100000, 1, NULL, NULL, 1651804961, 1, 1),
(4, 4, 'Làm việc riêng trong giờ làm việc', 100000, 1, NULL, NULL, 1651817038, 1, 1),
(5, 2, 'test', 20000, 0, 1651803932, 1, 1651804502, 1, 1),
(6, 4, 'Thưởng doanh số', 100000, 0, 1651803946, 1, 1651805180, 1, 0),
(7, 3, 'Thưởng ngày', 400000, 0, 1651804878, 1, 1651805170, 1, 0),
(8, 3, 'Thưởng tiền ốm', 300000, 0, 1651804968, 1, 1651805157, 1, 0),
(9, 2, 'Thưởng doanh số', 200000, 0, 1651805037, 1, 1651805141, 1, 0),
(10, 2, 'Đi làm muộn', 100000, 1, 1651805122, 1, NULL, NULL, 0),
(11, 5, 'kehn thưởng', 50000, 0, 1651901703, 1, NULL, NULL, 0),
(12, 5, 'Đi làm muônk', 100000, 1, 1651901975, 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `loai_nhan_vien`
--

CREATE TABLE `loai_nhan_vien` (
  `id` int(11) NOT NULL,
  `ten_loai` varchar(255) DEFAULT NULL,
  `mo_ta` text DEFAULT NULL,
  `ngay_tao` int(11) DEFAULT NULL,
  `nguoi_tao` int(11) DEFAULT NULL,
  `ngay_sua` int(11) DEFAULT NULL,
  `nguoi_sua` int(11) DEFAULT NULL,
  `xoa` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loai_nhan_vien`
--

INSERT INTO `loai_nhan_vien` (`id`, `ten_loai`, `mo_ta`, `ngay_tao`, `nguoi_tao`, `ngay_sua`, `nguoi_sua`, `xoa`) VALUES
(1, 'Nhân viên chính thức', NULL, NULL, NULL, NULL, NULL, 0),
(2, 'Nhân viên thời vụ', NULL, NULL, NULL, NULL, NULL, 0),
(3, 'ds2', 'ds2', 1651556419, 1, 1651556423, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `luong`
--

CREATE TABLE `luong` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `luong_gio` int(11) DEFAULT NULL,
  `ngay_tao` int(11) DEFAULT NULL,
  `nguoi_tao` int(11) DEFAULT NULL,
  `ngay_sua` int(11) DEFAULT NULL,
  `nguoi_sua` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `luong`
--

INSERT INTO `luong` (`id`, `user_id`, `luong_gio`, `ngay_tao`, `nguoi_tao`, `ngay_sua`, `nguoi_sua`) VALUES
(1, 2, 15000, NULL, NULL, NULL, NULL),
(2, 3, 20000, NULL, NULL, 1651558372, 1),
(4, 4, 23000, 1651558453, 1, 1651558464, 1),
(5, 5, 18000, 1651558459, 1, NULL, NULL),
(6, 6, 20000, 1652077289, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_09_17_143645_create_shop_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `phong_ban`
--

CREATE TABLE `phong_ban` (
  `id` int(11) NOT NULL,
  `ten_phong_ban` varchar(255) DEFAULT NULL,
  `mo_ta` text DEFAULT NULL,
  `ngay_tao` int(11) DEFAULT NULL,
  `nguoi_tao` int(11) DEFAULT NULL,
  `ngay_sua` int(11) DEFAULT NULL,
  `nguoi_sua` int(11) DEFAULT NULL,
  `xoa` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `phong_ban`
--

INSERT INTO `phong_ban` (`id`, `ten_phong_ban`, `mo_ta`, `ngay_tao`, `nguoi_tao`, `ngay_sua`, `nguoi_sua`, `xoa`) VALUES
(1, 'Maketing', NULL, NULL, NULL, NULL, NULL, 0),
(2, 'd', NULL, 1651549379, 1, 1651550044, 1, 1),
(3, 'bán hàng', NULL, 1651550451, 1, NULL, NULL, 0),
(4, 'đào tạo', NULL, 1651550446, 1, NULL, NULL, 0),
(5, 'ok', NULL, 1651549885, 1, 1651550164, 1, 1),
(6, 'Kế toán', NULL, 1651921147, 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `thong_tin_tai_khoan`
--

CREATE TABLE `thong_tin_tai_khoan` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ho_va_ten` varchar(255) DEFAULT NULL,
  `biet_danh` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gioi_tinh` int(11) DEFAULT NULL,
  `ngay_sinh` int(11) DEFAULT NULL,
  `noi_sinh` varchar(255) DEFAULT NULL,
  `tinh_trang_hon_nhan` int(11) DEFAULT NULL,
  `so_cmnd` int(11) DEFAULT NULL,
  `ngay_cap` int(11) DEFAULT NULL,
  `noi_cap` varchar(255) DEFAULT NULL,
  `nguyen_quan` varchar(255) DEFAULT NULL,
  `quoc_tich` varchar(255) DEFAULT NULL,
  `dan_toc` varchar(255) DEFAULT NULL,
  `ton_giao` varchar(255) DEFAULT NULL,
  `ho_khau` varchar(255) DEFAULT NULL,
  `tam_tru` varchar(255) DEFAULT NULL,
  `hinh_anh` varchar(255) DEFAULT NULL,
  `loai_nhan_vien` int(11) DEFAULT NULL,
  `trinh_do` int(11) DEFAULT NULL,
  `chuyen_mon` int(11) DEFAULT NULL,
  `phong_ban` int(11) DEFAULT NULL,
  `chuc_vu` int(11) DEFAULT NULL,
  `trang_thai` int(11) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `thong_tin_tai_khoan`
--

INSERT INTO `thong_tin_tai_khoan` (`id`, `user_id`, `ho_va_ten`, `biet_danh`, `email`, `gioi_tinh`, `ngay_sinh`, `noi_sinh`, `tinh_trang_hon_nhan`, `so_cmnd`, `ngay_cap`, `noi_cap`, `nguyen_quan`, `quoc_tich`, `dan_toc`, `ton_giao`, `ho_khau`, `tam_tru`, `hinh_anh`, `loai_nhan_vien`, `trinh_do`, `chuyen_mon`, `phong_ban`, `chuc_vu`, `trang_thai`, `is_deleted`) VALUES
(1, 1, NULL, NULL, 'admin2@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 2, 'Nguyễn Văn A', 'A', 'vinhhofb@gmail.com', 0, 1651410947, 'Liên Chiểu, Đà Nẵng', 0, 201783899, 1651410947, 'Công an Đà Nẵng', 'Đà Nẵng', 'Việt Nam', 'Kinh', 'Không', 'Đà Nẵng', 'Đà Nẵng', '1.jpg', 1, 1, 1, 1, 1, 1, 1),
(3, 3, 'Nguyễn Văn B', 'B', 'nguyenvanb@gmail.com', 0, 1651410947, 'Liên Chiểu, Đà Nẵng', 0, 201783890, 1651410947, 'Công an Đà Nẵng', 'Đà Nẵng', 'Việt Nam', 'Kinh', 'Không', 'Đà Nẵng', 'Đà Nẵng', '2.jpg', 1, 1, 1, 1, 1, 1, 0),
(4, 4, 'Hồ Hữu Vinh', 'hg', 'vinhhovinhho@gmail.com', 0, 1653436800, 'fd', 0, 555555555, 1652832000, 'dfdf', 'fd', 'fd', 'fd', 'fd', 'fd', 'fd', '1139285069.PNG', 1, 1, 1, 1, 2, 1, 0),
(5, 5, 'Nguyễn Văn C', 'c', 'vinh44344@donga.edu.vn', 0, 1652832000, 'da nang', 0, 203774747, 1652313600, 'df', 'g', 'hg', 'gh', 'gh', 'hg', 'hg', '114466771.PNG', 2, 10, 1, 1, 1, 1, 0),
(6, 6, 'Trần Công Khôi', 'Khôi hóm hỉnh', 'trancongkhoi2@gmail.com', 0, 1652140800, 'da', 1, 234438934, 1652832000, 'dg', 'ffg', 'fgfg', 'fggf', 'fgfg', 'fggf', 'fgfg', '1317775699.jpg', 1, 8, 2, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tien_do`
--

CREATE TABLE `tien_do` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cong_viec` int(11) DEFAULT NULL,
  `noi_dung` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tien_do`
--

INSERT INTO `tien_do` (`id`, `user_id`, `cong_viec`, `noi_dung`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 2, 1, 'Lên ý tưởng', 1651886716, NULL, NULL, NULL),
(2, 2, 1, 'Triển khai ý tưởng bước 1', 1651886716, NULL, NULL, NULL),
(3, 5, 6, 'ok', 1651901437, 5, NULL, NULL),
(4, 5, 6, 'Bắt đầu dự án', 1651901455, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trinh_do`
--

CREATE TABLE `trinh_do` (
  `id` int(11) NOT NULL,
  `ten_trinh_do` varchar(255) DEFAULT NULL,
  `mo_ta` text DEFAULT NULL,
  `ngay_tao` int(11) DEFAULT NULL,
  `nguoi_tao` int(11) DEFAULT NULL,
  `ngay_sua` int(11) DEFAULT NULL,
  `nguoi_sua` int(11) DEFAULT NULL,
  `xoa` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trinh_do`
--

INSERT INTO `trinh_do` (`id`, `ten_trinh_do`, `mo_ta`, `ngay_tao`, `nguoi_tao`, `ngay_sua`, `nguoi_sua`, `xoa`) VALUES
(1, 'Không', NULL, NULL, NULL, NULL, NULL, 0),
(2, '5', NULL, NULL, NULL, NULL, NULL, 0),
(3, '9', NULL, NULL, NULL, NULL, NULL, 0),
(4, '12', NULL, NULL, NULL, NULL, NULL, 0),
(5, 'Nghề', NULL, NULL, NULL, NULL, NULL, 0),
(6, 'Trung cấp', NULL, NULL, NULL, NULL, NULL, 0),
(7, 'Cao đẳng', NULL, NULL, NULL, NULL, NULL, 0),
(8, 'Đại học', NULL, NULL, NULL, NULL, NULL, 0),
(9, 'Thạc sĩ', NULL, NULL, NULL, NULL, NULL, 0),
(10, 'Tiến sĩ', NULL, NULL, NULL, NULL, NULL, 0),
(11, 's2', 's2', 1651551758, 1, 1651551797, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `password`, `role`, `active`, `remember_token`, `email_verified_at`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'admin', '0905663823', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 'aetXAO9uSALHmxmNNaJNTCIpJ9EC1hyc2NH8ImgJuGsFkhaH9fjvMdz23Ytf', NULL, 2147483647, 2147483647, 0),
(2, NULL, '0123467890', 'd41d8cd98f00b204e9800998ecf8427e', 2, 1, NULL, NULL, 2147483647, 2147483647, 1),
(3, NULL, '012345678', NULL, 2, 0, NULL, NULL, NULL, 1651474709, 0),
(4, 'Hồ Hữu Vinh', '5555555555', 'e10adc3949ba59abbe56e057f20f883e', 2, 1, 'fDuVVwwwOKQybuXkE9mt6MiylTPCsL2e9zpN8VvF0KZ1xLmboKlR78ykgntb', NULL, 1651473836, 1651547671, 0),
(5, NULL, '0799438886', 'ae962180c6b8ee5dfe2e1a50ed4a5384', 2, 1, 'RKFt333AzlI7k8qe6am7RIP2eFQMDyMJbEUJBbl3Joet8TY0hXbXc2wZuMqF', NULL, 1651547114, NULL, 0),
(6, 'Trần Công Khôi', '0123456788', 'ae962180c6b8ee5dfe2e1a50ed4a5384', 2, 1, 'U0U3odR5fUjImQjAOj5mUVhyVK2XyukL6QzrjCpqntYYenDEhoLwkJ7Zw09N', NULL, 1652075471, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_face`
--

CREATE TABLE `user_face` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `order_by` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_face`
--

INSERT INTO `user_face` (`id`, `name`, `image`, `order_by`, `user_id`, `created_at`, `updated_at`) VALUES
(75, 'Hồ Hữu Vinh', '1.jpg', 1, 4, 1652077615, NULL),
(76, 'Hồ Hữu Vinh', '2.jpg', 2, 4, 1652077622, NULL),
(77, 'Hồ Hữu Vinh', '3.jpg', 3, 4, 1652077629, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_track`
--

CREATE TABLE `user_track` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT 0,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_track`
--

INSERT INTO `user_track` (`id`, `user_id`, `type`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(11, 5, 0, 1651912895, NULL, NULL, NULL),
(12, 5, 1, 1651912987, NULL, NULL, NULL),
(13, 5, 0, 1651915417, NULL, NULL, NULL),
(14, 5, 1, 1651915451, NULL, NULL, NULL),
(21, 4, 0, 1652077776, NULL, NULL, NULL),
(22, 4, 1, 1652077808, NULL, NULL, NULL),
(23, 4, 0, 1652077837, NULL, NULL, NULL),
(24, 4, 1, 1652077875, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_mail_campaign_detail`
--
ALTER TABLE `admin_mail_campaign_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_mail_config`
--
ALTER TABLE `admin_mail_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_mail_template`
--
ALTER TABLE `admin_mail_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bang_tinh_luong`
--
ALTER TABLE `bang_tinh_luong`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chuc_vu`
--
ALTER TABLE `chuc_vu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chuyen_mon`
--
ALTER TABLE `chuyen_mon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_favorites`
--
ALTER TABLE `ch_favorites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ch_messages`
--
ALTER TABLE `ch_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cong_viec`
--
ALTER TABLE `cong_viec`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `khen_thuong_ky_luat`
--
ALTER TABLE `khen_thuong_ky_luat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loai_nhan_vien`
--
ALTER TABLE `loai_nhan_vien`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `luong`
--
ALTER TABLE `luong`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `phong_ban`
--
ALTER TABLE `phong_ban`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thong_tin_tai_khoan`
--
ALTER TABLE `thong_tin_tai_khoan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tien_do`
--
ALTER TABLE `tien_do`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trinh_do`
--
ALTER TABLE `trinh_do`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_face`
--
ALTER TABLE `user_face`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_track`
--
ALTER TABLE `user_track`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_mail_campaign_detail`
--
ALTER TABLE `admin_mail_campaign_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `admin_mail_config`
--
ALTER TABLE `admin_mail_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin_mail_template`
--
ALTER TABLE `admin_mail_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bang_tinh_luong`
--
ALTER TABLE `bang_tinh_luong`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `chuc_vu`
--
ALTER TABLE `chuc_vu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `chuyen_mon`
--
ALTER TABLE `chuyen_mon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cong_viec`
--
ALTER TABLE `cong_viec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `khen_thuong_ky_luat`
--
ALTER TABLE `khen_thuong_ky_luat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `loai_nhan_vien`
--
ALTER TABLE `loai_nhan_vien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `luong`
--
ALTER TABLE `luong`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phong_ban`
--
ALTER TABLE `phong_ban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `thong_tin_tai_khoan`
--
ALTER TABLE `thong_tin_tai_khoan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tien_do`
--
ALTER TABLE `tien_do`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `trinh_do`
--
ALTER TABLE `trinh_do`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_face`
--
ALTER TABLE `user_face`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `user_track`
--
ALTER TABLE `user_track`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
