<?php

namespace App\Http\Controllers\Admin\Specialize;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\User;

class SpecializeController extends Controller
{   
    public function ListStaff($id,Request $request){
        $GetListStaffs = DB::table('thong_tin_tai_khoan')
        ->where('chuyen_mon',$id)
        ->where('ho_va_ten','!=',null)
        ->orderBy('id', 'DESC');

        if(isset($request->keyword)){
            $GetListStaffs=$GetListStaffs
            ->where('user_id',$request->keyword)
            ->orWhere('so_cmnd',$request->keyword)
            ->where('phong_ban',$id)
            ->where('ho_va_ten','!=',null)
            ->orWhere('ho_va_ten',$request->keyword)
            ->where('phong_ban',$id)
            ->where('ho_va_ten','!=',null);
        };
        $GetListStaffs=$GetListStaffs->paginate(15);

        return view('Admin.StaffManage.ListStaffDetail',
            [
                'GetListStaffs'=>$GetListStaffs

            ]
        );
    }

    public function DeleteSpecialize($id){
        DB::table('chuyen_mon')->where('id',$id)->update(
            [   
                'xoa'=>1,
                'ngay_sua'=>time(),
                'nguoi_sua'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-chuyen-mon');
    }

    public function ListSpecialize(Request $request){
        $GetSpecializes = DB::table('chuyen_mon')
        ->where('xoa',0)
        ->orderBy('id', 'DESC');

        if(isset($request->keyword)){
            $GetSpecializes=$GetSpecializes
            ->where('ten_chuyen_mon',$request->keyword);
        }
        $GetSpecializes=$GetSpecializes->paginate(15);

        $getUsers = DB::table('thong_tin_tai_khoan')->where('chuyen_mon','!=',null)->get('chuyen_mon');

        return view('Admin.Specialize.ListSpecialize',
            [
                'GetSpecializes'=>$GetSpecializes,
                'getUsers'=>$getUsers
            ]
        );
    }
    public function AddSpecialize(){
        return view('Admin.Specialize.AddSpecialize');
    }

    public function PostAddSpecialize(Request $request){
        $validate = $request->validate([
            'ten_chuyen_mon' => 'required|max:255',
            'mo_ta'=>'max:255'
        ]);
        DB::table('chuyen_mon')->insert(
            [   
                'ten_chuyen_mon'=>$request->ten_chuyen_mon,
                'mo_ta'=>$request->mo_ta,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-chuyen-mon');
    }

    public function EditSpecialize($id){
        $getSpecialize = DB::table('chuyen_mon')->where('id',$id)->first();
        return view('Admin.Specialize.EditSpecialize',['getSpecialize'=>$getSpecialize,'id'=>$id]);
    }
    public function PostEditSpecialize($id,Request $request){
        $validate = $request->validate([
            'ten_chuyen_mon' => 'required|max:255',
            'mo_ta'=>'max:255'
        ]);
        DB::table('chuyen_mon')->where('id',$id)->update(
            [   
                'ten_chuyen_mon'=>$request->ten_chuyen_mon,
                'mo_ta'=>$request->mo_ta,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-chuyen-mon');
    }

    
}
