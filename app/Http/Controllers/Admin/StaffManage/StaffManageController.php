<?php

namespace App\Http\Controllers\Admin\StaffManage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use Validator;
use App\Models\User;

class StaffManageController extends Controller
{   

    public function DeleteStaff($id){
        DB::table('users')->where('id',$id)->update(['is_deleted'=>1]);
        DB::table('thong_tin_tai_khoan')->where('user_id',$id)->update(['is_deleted'=>1]);
        return back();
    }
    public function PostEditStaff($id,Request $request){
        $validate = $request->validate([

            'biet_danh' => 'required|max:255',
            'so_dien_thoai'=>'required|digits:10',
            'email'=>'required|email',
            'gioi_tinh'=>'required|integer',
            'ngay_sinh'=>'required|date',
            'noi_sinh'=>'required|max:255',
            'tinh_trang_hon_nhan'=>'required|integer',
            'so_cmnd'=>'required|integer',
            'ngay_cap'=>'required|date',
            'noi_cap'=>'required|max:255',
            'nguyen_quan'=>'required|max:255',
            'quoc_tich'=>'required|max:255',
            'dan_toc'=>'required|max:255',
            'ton_giao'=>'required|max:255',
            'ho_khau'=>'required|max:255',
            'tam_tru'=>'required|max:255',
            'loai_nhan_vien'=>'required|integer',
            'trinh_do'=>'required|integer',
            'chuyen_mon'=>'required|integer',
            'phong_ban'=>'required|integer',
            'chuc_vu'=>'required|integer',
        ]);
        
        if(isset($request->hinh_anh)){
            $new_name = rand() . '.' . $request->hinh_anh->getClientOriginalExtension();
            $request->hinh_anh->move(public_path('images/staff/'), $new_name);
            $request->hinh_anh=$new_name;
            DB::table('thong_tin_tai_khoan')->where('id',$id)->update(
                [
                    'hinh_anh'=>$request->hinh_anh,
                ]
            );
        }
        if(isset($request->password)){
            DB::table('users')->where('id',$id)(
                [
                    'password'=>md5($request->password),
                ]
            );
        }



        DB::table('thong_tin_tai_khoan')->where('id',$id)->update(
            [   
                'biet_danh'=>$request->biet_danh,
                'email'=>$request->email,
                'gioi_tinh'=>$request->gioi_tinh,
                'ngay_sinh'=>date(strtotime($request->ngay_sinh)),
                'noi_sinh'=>$request->noi_sinh,
                'tinh_trang_hon_nhan'=>$request->tinh_trang_hon_nhan,
                'so_cmnd'=>$request->so_cmnd,
                'ngay_cap'=>date(strtotime($request->ngay_cap)),
                'noi_cap'=>$request->noi_cap,
                'nguyen_quan'=>$request->nguyen_quan,
                'quoc_tich'=>$request->quoc_tich,
                'dan_toc'=>$request->dan_toc,
                'ton_giao'=>$request->ton_giao,
                'ho_khau'=>$request->ho_khau,
                'tam_tru'=>$request->tam_tru,
                'loai_nhan_vien'=>$request->loai_nhan_vien,
                'trinh_do'=>$request->trinh_do,
                'chuyen_mon'=>$request->chuyen_mon,
                'phong_ban'=>$request->phong_ban,
                'chuc_vu'=>$request->chuc_vu,
            ]
        ); 
        return redirect('admin/quan-ly-nhan-vien');
    }

    public function EditStaff($id){
        $getStaff = DB::table('thong_tin_tai_khoan')
        ->leftJoin('users','users.id','thong_tin_tai_khoan.user_id')
        ->select('thong_tin_tai_khoan.*','users.phone')
        ->where('thong_tin_tai_khoan.id',$id)->first();

        $loai_nhan_vien = DB::table('loai_nhan_vien')->get();
        $trinh_do=DB::table('trinh_do')->get();
        $chuyen_mon=DB::table('chuyen_mon')->get();
        $phong_ban=DB::table('phong_ban')->get();
        $chuc_vu=DB::table('chuc_vu')->get();
        return view('Admin.StaffManage.EditStaff',['getStaff'=>$getStaff,'loai_nhan_vien'=>$loai_nhan_vien,'trinh_do'=>$trinh_do,'chuyen_mon'=>$chuyen_mon,'phong_ban'=>$phong_ban,'chuc_vu'=>$chuc_vu,'id'=>$id]);
    }
    public function AddStaff(){
        $loai_nhan_vien = DB::table('loai_nhan_vien')->get();
        $trinh_do=DB::table('trinh_do')->get();
        $chuyen_mon=DB::table('chuyen_mon')->get();
        $phong_ban=DB::table('phong_ban')->get();
        $chuc_vu=DB::table('chuc_vu')->get();
        return view('Admin.StaffManage.AddStaff',['loai_nhan_vien'=>$loai_nhan_vien,'trinh_do'=>$trinh_do,'chuyen_mon'=>$chuyen_mon,'phong_ban'=>$phong_ban,'chuc_vu'=>$chuc_vu]);
    }

    public function PostAddStaff(Request $request){
        $validator = Validator::make($request->all(), [
            'ho_va_ten' => 'required|max:255',
            'biet_danh' => 'required|max:255',
            'hinh_anh'=>'image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048',
            'so_dien_thoai'=>'required|digits:10',
            'email'=>'required|email',
            'password'=>'required|min:6|max:50',
            'gioi_tinh'=>'required|integer',
            'ngay_sinh'=>'required|date',
            'noi_sinh'=>'required|max:255',
            'tinh_trang_hon_nhan'=>'required|integer',
            'so_cmnd'=>'required|integer',
            'ngay_cap'=>'required|date',
            'noi_cap'=>'required|max:255',
            'nguyen_quan'=>'required|max:255',
            'quoc_tich'=>'required|max:255',
            'dan_toc'=>'required|max:255',
            'ton_giao'=>'required|max:255',
            'ho_khau'=>'required|max:255',
            'tam_tru'=>'required|max:255',
            'loai_nhan_vien'=>'required|integer',
            'trinh_do'=>'required|integer',
            'chuyen_mon'=>'required|integer',
            'phong_ban'=>'required|integer',
            'chuc_vu'=>'required|integer',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }else{
            $checkUser = DB::table('users')->where('users.phone',$request->so_dien_thoai)->first();
            if($checkUser == null){
                if(isset($request->hinh_anh)){
                    $new_name = rand() . '.' . $request->hinh_anh->getClientOriginalExtension();
                    $request->hinh_anh->move(public_path('images/staff/'), $new_name);
                    $request->hinh_anh=$new_name;
                }
                $user_id = DB::table('users')->insertGetId(
                    [   
                        'name'=>$request->ho_va_ten,
                        'phone'=>$request->so_dien_thoai,
                        'password'=>md5($request->password),
                        'active'=>1,
                        'role'=>2,
                        'created_at'=>time()
                    ]
                );
                DB::table('thong_tin_tai_khoan')->insert(
                    [   
                        'user_id'=>$user_id,
                        'ho_va_ten'=>$request->ho_va_ten,
                        'biet_danh'=>$request->biet_danh,
                        'hinh_anh'=>$request->hinh_anh,
                        'email'=>$request->email,
                        'gioi_tinh'=>$request->gioi_tinh,
                        'ngay_sinh'=>date(strtotime($request->ngay_sinh)),
                        'noi_sinh'=>$request->noi_sinh,
                        'tinh_trang_hon_nhan'=>$request->tinh_trang_hon_nhan,
                        'so_cmnd'=>$request->so_cmnd,
                        'ngay_cap'=>date(strtotime($request->ngay_cap)),
                        'noi_cap'=>$request->noi_cap,
                        'nguyen_quan'=>$request->nguyen_quan,
                        'quoc_tich'=>$request->quoc_tich,
                        'dan_toc'=>$request->dan_toc,
                        'ton_giao'=>$request->ton_giao,
                        'ho_khau'=>$request->ho_khau,
                        'tam_tru'=>$request->tam_tru,
                        'loai_nhan_vien'=>$request->loai_nhan_vien,
                        'trinh_do'=>$request->trinh_do,
                        'chuyen_mon'=>$request->chuyen_mon,
                        'phong_ban'=>$request->phong_ban,
                        'chuc_vu'=>$request->chuc_vu,
                        'trang_thai'=>0
                    ]
                ); 
                return redirect('admin/quan-ly-nhan-vien');
            }else{
                return redirect()->back()->with('msg', 'Số điện thoại đã tồn tại');
            }
        } 
    }

    public function ListStaff(Request $request){
        $GetListStaffs = DB::table('thong_tin_tai_khoan')
        ->where('ho_va_ten','!=',null)
        ->where('is_deleted',0)
        ->orderBy('id', 'DESC');


        if(isset($request->keyword)){
            $GetListStaffs=$GetListStaffs
            ->where('user_id',$request->keyword)
            ->orWhere('so_cmnd',$request->keyword)
            ->where('ho_va_ten','!=',null)
            ->where('is_deleted',0)
            ->orWhere('ho_va_ten',$request->keyword)
            ->where('ho_va_ten','!=',null)
            ->where('is_deleted',0);
        }
        $GetListStaffs=$GetListStaffs->paginate(15);


        return view('Admin.StaffManage.ListStaff',
            [
                'GetListStaffs'=>$GetListStaffs,
            ]
        );
    }

    public function StaffDetail($id){
        $GetStaffs = DB::table('thong_tin_tai_khoan')
        ->leftJoin('loai_nhan_vien','loai_nhan_vien.id','thong_tin_tai_khoan.loai_nhan_vien')
        ->leftJoin('trinh_do','trinh_do.id','thong_tin_tai_khoan.trinh_do')
        ->leftJoin('chuyen_mon','chuyen_mon.id','thong_tin_tai_khoan.chuyen_mon')
        ->leftJoin('phong_ban','phong_ban.id','thong_tin_tai_khoan.phong_ban')
        ->leftJoin('chuc_vu','chuc_vu.id','thong_tin_tai_khoan.chuc_vu')
        ->leftJoin('users','users.id','thong_tin_tai_khoan.user_id')
        ->select('thong_tin_tai_khoan.*','loai_nhan_vien.ten_loai as loai_nhan_vien','trinh_do.ten_trinh_do as trinh_do','chuyen_mon.ten_chuyen_mon as chuyen_mon','phong_ban.ten_phong_ban as phong_ban','chuc_vu.ten_chuc_vu as chuc_vu','users.phone as so_dien_thoai')
        ->where('thong_tin_tai_khoan.id', $id)
        ->first();


        return view('Admin.StaffManage.StaffDetail',
            [
                'GetStaffs'=>$GetStaffs,
            ]
        );
    }

    public function BlockUnBlockUser($id){

        $getUser = DB::table('users')->where('id',$id)->first();

        if($getUser != null){
            if($getUser->active == 0){
                DB::table('users')->where('id',$id)->update(['active'=>1,'updated_at'=>time()]);
                return back();
            }else if($getUser->active == 1){
                DB::table('users')->where('id',$id)->update(['active'=>0,'updated_at'=>time()]);
                return back();
            }             
        }

        
    }
}
