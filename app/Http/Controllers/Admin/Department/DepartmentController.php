<?php

namespace App\Http\Controllers\Admin\Department;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\User;

class DepartmentController extends Controller
{   
    public function ListStaff($id,Request $request){
        $GetListStaffs = DB::table('thong_tin_tai_khoan')
        ->where('phong_ban',$id)
        ->where('ho_va_ten','!=',null)
        ->orderBy('id', 'DESC');

        if(isset($request->keyword)){
            $GetListStaffs=$GetListStaffs
            ->where('user_id',$request->keyword)
            ->orWhere('so_cmnd',$request->keyword)
            ->where('phong_ban',$id)
            ->where('ho_va_ten','!=',null)
            ->orWhere('ho_va_ten',$request->keyword)
            ->where('phong_ban',$id)
            ->where('ho_va_ten','!=',null);
        };
        $GetListStaffs=$GetListStaffs->paginate(15);

        return view('Admin.StaffManage.ListStaffDetail',
            [
                'GetListStaffs'=>$GetListStaffs

            ]
        );
    }
    public function DeleteDepartment($id){
        DB::table('phong_ban')->where('id',$id)->update(
            [   
                'xoa'=>1,
                'ngay_sua'=>time(),
                'nguoi_sua'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-phong-ban');
    }

    public function ListDepartment(Request $request){
        $GetDepartments = DB::table('phong_ban')
        ->where('xoa',0)
        ->orderBy('id', 'DESC');

        if(isset($request->keyword)){
            $GetDepartments=$GetDepartments
            ->where('ten_phong_ban',$request->keyword);
        }
        $GetDepartments=$GetDepartments->paginate(15);

        $getUsers = DB::table('thong_tin_tai_khoan')->where('phong_ban','!=',null)->get('phong_ban');

        return view('Admin.Department.ListDepartment',
            [
                'GetDepartments'=>$GetDepartments,
                'getUsers'=>$getUsers
            ]
        );
    }
    public function AddDepartment(){
        return view('Admin.Department.AddDepartment');
    }

    public function PostAddDepartment(Request $request){
        $validate = $request->validate([
            'ten_phong_ban' => 'required|max:255',
            'mo_ta'=>'max:255'
        ]);
        DB::table('phong_ban')->insert(
            [   
                'ten_phong_ban'=>$request->ten_phong_ban,
                'mo_ta'=>$request->mo_ta,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-phong-ban');
    }

    public function EditDepartment($id){
        $getDepartment = DB::table('phong_ban')->where('id',$id)->first();
        return view('Admin.Department.EditDepartment',['getDepartment'=>$getDepartment,'id'=>$id]);
    }
    public function PostEditDepartment($id,Request $request){
        $validate = $request->validate([
            'ten_phong_ban' => 'required|max:255',
            'mo_ta'=>'max:255'
        ]);
        DB::table('phong_ban')->where('id',$id)->update(
            [   
                'ten_phong_ban'=>$request->ten_phong_ban,
                'mo_ta'=>$request->mo_ta,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-phong-ban');
    }

    
}
