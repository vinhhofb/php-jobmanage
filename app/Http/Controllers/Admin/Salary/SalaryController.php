<?php

namespace App\Http\Controllers\Admin\Salary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\User;

class SalaryController extends Controller
{   

    public function ListSalaryStaffDetail($id){
        
        
        $GetTime = DB::table('user_track')->where('user_id',$id)->get();

        $GetSalary = DB::table('luong')->where('user_id',$id)->first('luong_gio');
        

        $countTime = 0;

        $checktime = array();
        for ($i=1; $i < count($GetTime); $i++) { 
            if($GetTime[$i]->type == 1){
                $countTime += $GetTime[$i]->created_at - $GetTime[$i-1]->created_at;
                array_push($checktime, [
                    'checkin' => $GetTime[$i-1]->created_at,
                    'checkout' => $GetTime[$i]->created_at,
                    'time'=>gmdate("H:i:s",$GetTime[$i]->created_at - $GetTime[$i-1]->created_at),  
                    'salary'=>($GetTime[$i]->created_at - $GetTime[$i-1]->created_at)/60/60*$GetSalary->luong_gio
                ]);
            }
        }
      
        
        $time=gmdate("H:i:s",$countTime);
        $salary = $countTime/60/60*$GetSalary->luong_gio;
        
        $mounth = date('n');
        return view('Admin.Salary.ListSalaryStaffDetail',
            [
                'checktime'=>$checktime,
                'time'=>$time,
                'salary'=>$salary,
                'mounth'=>$mounth,
                'GetSalary'=>$GetSalary->luong_gio
            ]
        );
    
    }

    public function ListSalaryStaff(){
        $getStaff = DB::table('users')
        ->leftJoin('thong_tin_tai_khoan','thong_tin_tai_khoan.user_id','users.id')
        ->leftJoin('luong','luong.user_id','users.id')
        ->leftJoin('chuc_vu','chuc_vu.id','thong_tin_tai_khoan.chuc_vu')
        ->select('users.id','thong_tin_tai_khoan.ho_va_ten','luong.luong_gio','chuc_vu.ten_chuc_vu')
        ->where('users.role',2)
        ->orderBy('users.id','desc')
        ->paginate(15);
        $checktime=array();
        for ($i=0; $i < count($getStaff) ; $i++) { 
            $GetTime = DB::table('user_track')->where('user_id',$getStaff[$i]->id)->get();
            $GetSalary = DB::table('luong')->where('user_id',$getStaff[$i]->id)->first('luong_gio');
            $total =0;
            for ($j=1; $j < count($GetTime); $j++) { 
                if($GetTime[$j]->type == 1){
                    $total+=($GetTime[$j]->created_at - $GetTime[$j-1]->created_at)/60/60*$GetSalary->luong_gio;
                }
            }
            array_push($checktime, [
                'id'=>$getStaff[$i]->id,  
                'salary'=>$total 
            ]);
        }
        return view('Admin.Salary.ListSalaryStaff',['getStaff'=>$getStaff,'checktime'=>$checktime]);
        
    }



    public function ListSalary(Request $request){
        $GetSalarys = DB::table('users')
        ->leftJoin('luong','luong.user_id','users.id')
        ->leftJoin('thong_tin_tai_khoan','thong_tin_tai_khoan.id','users.id')
        ->leftJoin('chuc_vu','chuc_vu.id','thong_tin_tai_khoan.chuc_vu')
        ->leftJoin('trinh_do','trinh_do.id','thong_tin_tai_khoan.trinh_do')
        ->select('thong_tin_tai_khoan.ho_va_ten','users.id','chuc_vu.ten_chuc_vu','luong.luong_gio','luong.ngay_tao','trinh_do.ten_trinh_do')
        ->orderBy('users.id', 'DESC')
        ->where('thong_tin_tai_khoan.ho_va_ten','!=',null)
        ;

        if(isset($request->keyword)){
            $GetSalarys=$GetSalarys
            ->where('users.phone',$request->keyword)
            ->orWhere('thong_tin_tai_khoan.ho_va_ten',$request->keyword)
            ->orWhere('thong_tin_tai_khoan.so_cmnd',$request->keyword);
        }
        $GetSalarys=$GetSalarys->paginate(15);

        

        return view('Admin.Salary.ListSalary',
            [
                'GetSalarys'=>$GetSalarys,

            ]
        );
    }
    

    

    public function EditSalary($id){
        $getSalary = DB::table('luong')->where('user_id',$id)->first();
        return view('Admin.Salary.EditSalary',['getSalary'=>$getSalary,'id'=>$id]);
    }
    public function PostEditSalary($id,Request $request){
        $validate = $request->validate([
            'luong_gio' => 'required|integer',
        ]);
        $getSalary = DB::table('luong')->where('user_id',$id)->first();

        if($getSalary == null){
            DB::table('luong')->insert(
                [   
                    'user_id'=>$id,
                    'luong_gio'=>$request->luong_gio,
                    'ngay_tao'=>time(),
                    'nguoi_tao'=>Auth::user()->id,
                ]
            ); 
        }else{
            DB::table('luong')->where('user_id',$id)->update(
                [   
                    'luong_gio'=>$request->luong_gio,
                    'ngay_sua'=>time(),
                    'nguoi_sua'=>Auth::user()->id,
                ]
            ); 
        }
        
        return redirect('admin/quan-ly-luong');
    }

    
}
