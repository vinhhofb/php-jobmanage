<?php

namespace App\Http\Controllers\Admin\Bonus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\User;

class BonusController extends Controller
{   
    public function ListBonus(Request $request){
        $GetBonus = DB::table('khen_thuong_ky_luat')
        ->leftJoin('users','users.id','khen_thuong_ky_luat.user_id')
        ->leftJoin('thong_tin_tai_khoan','thong_tin_tai_khoan.id','users.id')
        ->leftJoin('chuc_vu','chuc_vu.id','thong_tin_tai_khoan.chuc_vu')
        ->leftJoin('trinh_do','trinh_do.id','thong_tin_tai_khoan.trinh_do')
        ->select('thong_tin_tai_khoan.ho_va_ten','chuc_vu.ten_chuc_vu','khen_thuong_ky_luat.*')
        ->orderBy('khen_thuong_ky_luat.id', 'DESC')
        ->where('khen_thuong_ky_luat.loai',0)
        ->where('khen_thuong_ky_luat.xoa',0)
        ;

        if(isset($request->keyword)){
            $GetBonus=$GetBonus
            ->where('users.phone',$request->keyword)
            ->orWhere('thong_tin_tai_khoan.ho_va_ten',$request->keyword)
            ->where('khen_thuong_ky_luat.loai',0)
            ->where('khen_thuong_ky_luat.xoa',0)
            ->orWhere('thong_tin_tai_khoan.so_cmnd',$request->keyword)
            ->where('khen_thuong_ky_luat.loai',0)
            ->where('khen_thuong_ky_luat.xoa',0);
        }
        $GetBonus=$GetBonus->paginate(15);

        

        return view('Admin.Bonus.ListBonus',
            [
                'GetBonus'=>$GetBonus,

            ]
        );
    }
    

    

    public function EditBonus($id){
        $getBonus = DB::table('khen_thuong_ky_luat')->where('id',$id)->first();
        return view('Admin.Bonus.EditBonus',['getBonus'=>$getBonus,'id'=>$id]);
    }
    public function PostEditBonus($id,Request $request){
        $validate = $request->validate([
            'mo_ta' => 'required',
            'gia_tri' => 'required|integer',
        ]);
        $getBonus = DB::table('luong')->where('user_id',$id)->first();


        DB::table('khen_thuong_ky_luat')->where('id',$id)->update(
            [   
                'mo_ta'=>$request->mo_ta,
                'gia_tri'=>$request->gia_tri,
                'loai'=>0,
                'ngay_sua'=>time(),
                'nguoi_sua'=>Auth::user()->id,
            ]
        ); 
        
        
        return redirect('admin/khen-thuong');
    }

    public function AddBonus(){
        $getUsers = DB::table('users')->join('thong_tin_tai_khoan','thong_tin_tai_khoan.user_id','users.id')
        ->where('role',2)->get();

        return view('Admin.Bonus.AddBonus',['getUsers'=>$getUsers]);
    }
    public function PostAddBonus(Request $request){
        $validate = $request->validate([
            'user_id' => 'required|integer',
            'mo_ta' => 'required',
            'gia_tri' => 'required|integer',
        ]);
        
        DB::table('khen_thuong_ky_luat')->insert(
            [   
                'user_id'=>$request->user_id,
                'mo_ta'=>$request->mo_ta,
                'gia_tri'=>$request->gia_tri,
                'loai'=>0,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        
        return redirect('admin/khen-thuong');
    }

    public function DeleteBonus($id){

        DB::table('khen_thuong_ky_luat')->where('id',$id)->update(
            [   
                'xoa'=>1,
                'ngay_sua'=>time(),
                'nguoi_sua'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/khen-thuong');

    }
    
}
