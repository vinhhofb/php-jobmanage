<?php

namespace App\Http\Controllers\Admin\Position;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\User;

class PositionController extends Controller
{   

    public function ListStaff($id,Request $request){
        $GetListStaffs = DB::table('thong_tin_tai_khoan')
        ->where('chuc_vu',$id)
        ->where('ho_va_ten','!=',null)
        ->orderBy('id', 'DESC');

        if(isset($request->keyword)){
            $GetListStaffs=$GetListStaffs
            ->where('user_id',$request->keyword)
            ->orWhere('so_cmnd',$request->keyword)
            ->where('phong_ban',$id)
            ->where('ho_va_ten','!=',null)
            ->orWhere('ho_va_ten',$request->keyword)
            ->where('phong_ban',$id)
            ->where('ho_va_ten','!=',null);
        };
        $GetListStaffs=$GetListStaffs->paginate(15);

        return view('Admin.StaffManage.ListStaffDetail',
            [
                'GetListStaffs'=>$GetListStaffs

            ]
        );
    }

    public function DeletePosition($id){
        DB::table('chuc_vu')->where('id',$id)->update(
            [   
                'xoa'=>1,
                'ngay_sua'=>time(),
                'nguoi_sua'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-chuc-vu');
    }

    public function ListPosition(Request $request){
        $GetPositions = DB::table('chuc_vu')
        ->where('xoa',0)
        ->orderBy('id', 'DESC');

        if(isset($request->keyword)){
            $GetPositions=$GetPositions
            ->where('ten_chuc_vu',$request->keyword);
        }
        $GetPositions=$GetPositions->paginate(15);

        $getUsers = DB::table('thong_tin_tai_khoan')->where('chuc_vu','!=',null)->get('chuc_vu');

        return view('Admin.Position.ListPosition',
            [
                'GetPositions'=>$GetPositions,
                'getUsers'=>$getUsers
            ]
        );
    }
    public function AddPosition(){
        return view('Admin.Position.AddPosition');
    }

    public function PostAddPosition(Request $request){
        $validate = $request->validate([
            'ten_chuc_vu' => 'required|max:255',
            'mo_ta'=>'max:255'
        ]);
        DB::table('chuc_vu')->insert(
            [   
                'ten_chuc_vu'=>$request->ten_chuc_vu,
                'mo_ta'=>$request->mo_ta,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-chuc-vu');
    }

    public function EditPosition($id){
        $getPosition = DB::table('chuc_vu')->where('id',$id)->first();
        return view('Admin.Position.EditPosition',['getPosition'=>$getPosition,'id'=>$id]);
    }
    public function PostEditPosition($id,Request $request){
        $validate = $request->validate([
            'ten_chuc_vu' => 'required|max:255',
            'mo_ta'=>'max:255'
        ]);
        DB::table('chuc_vu')->where('id',$id)->update(
            [   
                'ten_chuc_vu'=>$request->ten_chuc_vu,
                'mo_ta'=>$request->mo_ta,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-chuc-vu');
    }

    
}
