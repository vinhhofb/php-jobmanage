<?php

namespace App\Http\Controllers\Admin\Payroll;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\User;

class PayrollController extends Controller
{   



    public function ListPayroll(Request $request){
        $GetPayrolls = DB::table('users')
        ->leftJoin('luong','luong.user_id','users.id')
        ->leftJoin('thong_tin_tai_khoan','thong_tin_tai_khoan.id','users.id')
        ->leftJoin('chuc_vu','chuc_vu.id','thong_tin_tai_khoan.chuc_vu')
        ->leftJoin('trinh_do','trinh_do.id','thong_tin_tai_khoan.trinh_do')
        ->select('thong_tin_tai_khoan.ho_va_ten','users.id','chuc_vu.ten_chuc_vu','luong.luong_gio','luong.ngay_tao','trinh_do.ten_trinh_do')
        ->orderBy('users.id', 'DESC')
        ->where('thong_tin_tai_khoan.ho_va_ten','!=',null)
        ;

        if(isset($request->keyword)){
            $GetPayrolls=$GetPayrolls
            ->where('users.phone',$request->keyword)
            ->orWhere('thong_tin_tai_khoan.ho_va_ten',$request->keyword)
            ->orWhere('thong_tin_tai_khoan.so_cmnd',$request->keyword);
        }
        $GetPayrolls=$GetPayrolls->paginate(15);

        

        return view('Admin.Payroll.ListPayroll',
            [
                'GetPayrolls'=>$GetPayrolls,

            ]
        );
    }
    

    

    public function EditPayroll($id){
        $getPayroll = DB::table('luong')->where('user_id',$id)->first();
        return view('Admin.Payroll.EditPayroll',['getPayroll'=>$getPayroll,'id'=>$id]);
    }
    public function PostEditPayroll($id,Request $request){
        $validate = $request->validate([
            'luong_gio' => 'required|integer',
        ]);
        $getPayroll = DB::table('luong')->where('user_id',$id)->first();

        if($getPayroll == null){
            DB::table('luong')->insert(
                [   
                    'user_id'=>$id,
                    'luong_gio'=>$request->luong_gio,
                    'ngay_tao'=>time(),
                    'nguoi_tao'=>Auth::user()->id,
                ]
            ); 
        }else{
            DB::table('luong')->where('user_id',$id)->update(
                [   
                    'luong_gio'=>$request->luong_gio,
                    'ngay_sua'=>time(),
                    'nguoi_sua'=>Auth::user()->id,
                ]
            ); 
        }
        
        return redirect('admin/quan-ly-luong');
    }

    
}
