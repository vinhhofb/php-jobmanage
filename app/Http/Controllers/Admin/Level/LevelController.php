<?php

namespace App\Http\Controllers\Admin\Level;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\User;

class LevelController extends Controller
{   

    public function ListStaff($id,Request $request){
        $GetListStaffs = DB::table('thong_tin_tai_khoan')
        ->where('trinh_do',$id)
        ->where('ho_va_ten','!=',null)
        ->orderBy('id', 'DESC');

        if(isset($request->keyword)){
            $GetListStaffs=$GetListStaffs
            ->where('user_id',$request->keyword)
            ->orWhere('so_cmnd',$request->keyword)
            ->where('phong_ban',$id)
            ->where('ho_va_ten','!=',null)
            ->orWhere('ho_va_ten',$request->keyword)
            ->where('phong_ban',$id)
            ->where('ho_va_ten','!=',null);
        };
        $GetListStaffs=$GetListStaffs->paginate(15);

        return view('Admin.StaffManage.ListStaffDetail',
            [
                'GetListStaffs'=>$GetListStaffs

            ]
        );
    }

    public function DeleteLevel($id){
        DB::table('trinh_do')->where('id',$id)->update(
            [   
                'xoa'=>1,
                'ngay_sua'=>time(),
                'nguoi_sua'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-trinh-do');
    }

    public function ListLevel(Request $request){
        $GetLevels = DB::table('trinh_do')
        ->where('xoa',0)
        ->orderBy('id', 'DESC');

        if(isset($request->keyword)){
            $GetLevels=$GetLevels
            ->where('ten_trinh_do',$request->keyword);
        }
        $GetLevels=$GetLevels->paginate(15);

        $getUsers = DB::table('thong_tin_tai_khoan')->where('trinh_do','!=',null)->get('trinh_do');

        return view('Admin.Level.ListLevel',
            [
                'GetLevels'=>$GetLevels,
                'getUsers'=>$getUsers
            ]
        );
    }
    public function AddLevel(){
        return view('Admin.Level.AddLevel');
    }

    public function PostAddLevel(Request $request){
        $validate = $request->validate([
            'ten_trinh_do' => 'required|max:255',
            'mo_ta'=>'max:255'
        ]);
        DB::table('trinh_do')->insert(
            [   
                'ten_trinh_do'=>$request->ten_trinh_do,
                'mo_ta'=>$request->mo_ta,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-trinh-do');
    }

    public function EditLevel($id){
        $getLevel = DB::table('trinh_do')->where('id',$id)->first();
        return view('Admin.Level.EditLevel',['getLevel'=>$getLevel,'id'=>$id]);
    }
    public function PostEditLevel($id,Request $request){
        $validate = $request->validate([
            'ten_trinh_do' => 'required|max:255',
            'mo_ta'=>'max:255'
        ]);
        DB::table('trinh_do')->where('id',$id)->update(
            [   
                'ten_trinh_do'=>$request->ten_trinh_do,
                'mo_ta'=>$request->mo_ta,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-trinh-do');
    }

    
}
