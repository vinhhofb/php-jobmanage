<?php

namespace App\Http\Controllers\Admin\Work;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\User;
use Mail;

class WorkController extends Controller
{   

    public function WorkDetail($id){
        $GetWork = DB::table('cong_viec')->where('id',$id)->first();
        $GetWorkDetail= DB::table('tien_do')->where('cong_viec',$id)->get();
        

        return view('Admin.Work.WorkDetail',['GetWork'=>$GetWork,'GetWorkDetail'=>$GetWorkDetail]);
    }
    public function ListWork(Request $request){
        $GetWork = DB::table('cong_viec')
        ->leftJoin('users','users.id','cong_viec.user_id')
        ->leftJoin('thong_tin_tai_khoan','thong_tin_tai_khoan.id','users.id')
        ->leftJoin('chuc_vu','chuc_vu.id','thong_tin_tai_khoan.chuc_vu')
        ->leftJoin('trinh_do','trinh_do.id','thong_tin_tai_khoan.trinh_do')
        ->select('thong_tin_tai_khoan.ho_va_ten','chuc_vu.ten_chuc_vu','cong_viec.*')
        ->orderBy('cong_viec.id', 'DESC')
        ->where('cong_viec.xoa',0);
        if(isset($request->keyword)){
            $GetWork=$GetWork
            ->where('users.phone',$request->keyword)
            ->orWhere('thong_tin_tai_khoan.ho_va_ten',$request->keyword)
            ->where('cong_viec.xoa',0)
            ->orWhere('thong_tin_tai_khoan.so_cmnd',$request->keyword)
            ->where('cong_viec.xoa',0);
        }
        $GetWork=$GetWork->paginate(15);
        return view('Admin.Work.ListWork',
            [
                'GetWork'=>$GetWork,
            ]
        );
    }
    

    

    public function EditWork($id){
        $getWork = DB::table('cong_viec')->where('id',$id)->first();
        return view('Admin.Work.EditWork',['getWork'=>$getWork,'id'=>$id]);
    }
    public function PostEditWork($id,Request $request){
        $validate = $request->validate([
            'ten_cong_viec' => 'required',
            'mo_ta' => 'required',
            'tu_ngay' => 'required',
            'den_ngay' => 'required',
        ]);
        $getWork = DB::table('cong_viec')->where('id',$id)->first();
        $request->user_id=$getWork->user_id;
        if(isset($request->email_notification)){
            $getEmailUser = DB::table('thong_tin_tai_khoan')->where('user_id',$request->user_id)->first();

            $getEmailTemplate = DB::table('admin_mail_template')
            ->where('id','=',2)
            ->first();
            $getEmailConfig = DB::table('admin_mail_config')
            ->where('id','=',1)
            ->first();
            try{
                //Bỏ thông tin mail config vào swift smtp
                $transport = (new \Swift_SmtpTransport($getEmailConfig->mail_host,$getEmailConfig->mail_port))
                ->setUsername($getEmailConfig->mail_username)->setPassword($getEmailConfig->mail_password)->setEncryption($getEmailConfig->mail_encryption);
                $mailer = new \Swift_Mailer($transport);
                //thiết lập tiêu đề, nội dung mail gửi
                $message = (new \Swift_Message($getEmailTemplate->template_title))
                ->setFrom($getEmailConfig->mail_username)
                ->setTo($getEmailUser->email)
                ->addPart(
                  $getEmailTemplate->template_content,
                  'text/html'
              );
                $mailer->send($message);
            }catch (\Swift_TransportException $transportExp){
            }
            DB::table('cong_viec')->where('cong_viec.id',$id)->update(
                [
                    'ten_cong_viec'=>$request->ten_cong_viec,
                    'mo_ta'=>$request->mo_ta,
                    'tu_ngay'=>date(strtotime($request->tu_ngay)),
                    'den_ngay'=>date(strtotime($request->den_ngay)),
                    'ngay_sua'=>time(),
                    'nguoi_sua'=>Auth::user()->id
                ]
            );
            return redirect('admin/quan-ly-cong-viec');
        }
    }

    public function AddWork(){
        $getUsers = DB::table('users')->join('thong_tin_tai_khoan','thong_tin_tai_khoan.user_id','users.id')
        ->where('role',2)->get();

        return view('Admin.Work.AddWork',['getUsers'=>$getUsers]);
    }
    public function PostAddWork(Request $request){
        $validate = $request->validate([
            'user_id' => 'required|integer',
            'ten_cong_viec' => 'required',
            'mo_ta' => 'required',
            'tu_ngay' => 'required',
            'den_ngay' => 'required',
        ]);
        if(isset($request->email_notification)){
            $getEmailUser = DB::table('thong_tin_tai_khoan')->where('user_id',$request->user_id)->first();

            $getEmailTemplate = DB::table('admin_mail_template')
            ->where('id','=',1)
            ->first();
            $getEmailConfig = DB::table('admin_mail_config')
            ->where('id','=',1)
            ->first();
            try{
                //Bỏ thông tin mail config vào swift smtp
                $transport = (new \Swift_SmtpTransport($getEmailConfig->mail_host,$getEmailConfig->mail_port))
                ->setUsername($getEmailConfig->mail_username)->setPassword($getEmailConfig->mail_password)->setEncryption($getEmailConfig->mail_encryption);
                $mailer = new \Swift_Mailer($transport);
                //thiết lập tiêu đề, nội dung mail gửi
                $message = (new \Swift_Message($getEmailTemplate->template_title))
                ->setFrom($getEmailConfig->mail_username)
                ->setTo($getEmailUser->email)
                ->addPart(
                  $getEmailTemplate->template_content,
                  'text/html'
              );
                $mailer->send($message);
            }catch (\Swift_TransportException $transportExp){
            }
            DB::table('cong_viec')->insert(
                [
                    'user_id'=>$request->user_id,
                    'ten_cong_viec'=>$request->ten_cong_viec,
                    'mo_ta'=>$request->mo_ta,
                    'tu_ngay'=>date(strtotime($request->tu_ngay)),
                    'den_ngay'=>date(strtotime($request->den_ngay)),
                    'ngay_tao'=>time(),
                    'nguoi_tao'=>Auth::user()->id
                ]
            );
            return redirect('admin/quan-ly-cong-viec');
        }else{
            DB::table('cong_viec')->insert(
                [
                    'user_id'=>$request->user_id,
                    'ten_cong_viec'=>$request->ten_cong_viec,
                    'mo_ta'=>$request->mo_ta,
                    'tu_ngay'=>date(strtotime($request->tu_ngay)),
                    'den_ngay'=>date(strtotime($request->den_ngay)),
                    'ngay_tao'=>time(),
                    'nguoi_tao'=>Auth::user()->id
                ]
            );
            return redirect('admin/quan-ly-cong-viec');
        }
        
    }

    public function DeleteWork($id){

        DB::table('cong_viec')->where('id',$id)->update(
            [   
                'xoa'=>1,
                'ngay_sua'=>time(),
                'nguoi_sua'=>Auth::user()->id,
            ]
        ); 
        return back();

    }
    
}
