<?php

namespace App\Http\Controllers\Admin\TypeStaff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\User;

class TypeStaffController extends Controller
{   

    public function ListStaff($id,Request $request){
        $GetListStaffs = DB::table('thong_tin_tai_khoan')
        ->where('loai_nhan_vien',$id)
        ->where('ho_va_ten','!=',null)
        ->orderBy('id', 'DESC');

        if(isset($request->keyword)){
            $GetListStaffs=$GetListStaffs
            ->where('user_id',$request->keyword)
            ->orWhere('so_cmnd',$request->keyword)
            ->where('phong_ban',$id)
            ->where('ho_va_ten','!=',null)
            ->orWhere('ho_va_ten',$request->keyword)
            ->where('phong_ban',$id)
            ->where('ho_va_ten','!=',null);
        };
        $GetListStaffs=$GetListStaffs->paginate(15);

        return view('Admin.StaffManage.ListStaffDetail',
            [
                'GetListStaffs'=>$GetListStaffs

            ]
        );
    }


    public function DeleteTypeStaff($id){
        DB::table('loai_nhan_vien')->where('id',$id)->update(
            [   
                'xoa'=>1,
                'ngay_sua'=>time(),
                'nguoi_sua'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-loai-nhan-vien');
    }

    public function ListTypeStaff(Request $request){
        $GetTypeStaffs = DB::table('loai_nhan_vien')
        ->where('xoa',0)
        ->orderBy('id', 'DESC');

        if(isset($request->keyword)){
            $GetTypeStaffs=$GetTypeStaffs
            ->where('ten_loai',$request->keyword);
        }
        $GetTypeStaffs=$GetTypeStaffs->paginate(15);

        $getUsers = DB::table('thong_tin_tai_khoan')->where('loai_nhan_vien','!=',null)->get('loai_nhan_vien');
        

        return view('Admin.TypeStaff.ListTypeStaff',
            [
                'GetTypeStaffs'=>$GetTypeStaffs,
                'getUsers'=>$getUsers
            ]
        );
    }
    public function AddTypeStaff(){
        return view('Admin.TypeStaff.AddTypeStaff');
    }

    public function PostAddTypeStaff(Request $request){
        $validate = $request->validate([
            'ten_loai' => 'required|max:255',
            'mo_ta'=>'max:255'
        ]);
        DB::table('loai_nhan_vien')->insert(
            [   
                'ten_loai'=>$request->ten_loai,
                'mo_ta'=>$request->mo_ta,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-loai-nhan-vien');
    }

    public function EditTypeStaff($id){
        $getTypeStaff = DB::table('loai_nhan_vien')->where('id',$id)->first();
        return view('Admin.TypeStaff.EditTypeStaff',['getTypeStaff'=>$getTypeStaff,'id'=>$id]);
    }
    public function PostEditTypeStaff($id,Request $request){
        $validate = $request->validate([
            'ten_loai' => 'required|max:255',
            'mo_ta'=>'max:255'
        ]);
        DB::table('loai_nhan_vien')->where('id',$id)->update(
            [   
                'ten_loai'=>$request->ten_loai,
                'mo_ta'=>$request->mo_ta,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        return redirect('admin/quan-ly-loai-nhan-vien');
    }

    
}
