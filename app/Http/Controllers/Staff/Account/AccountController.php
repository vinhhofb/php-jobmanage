<?php

namespace App\Http\Controllers\Staff\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Redirect;
use DB;


class AccountController extends Controller
{   
    public function Logout(){
        if(Auth::user()){
            Auth::logout();
            return Redirect::to('/');
        }
    }
    public function Login(){
        if(Auth::user()){
            if(Auth::user()->role == 1){
                return Redirect::to('admin/quan-ly-nhan-vien');
            }else if(Auth::user()->role == 2){
                return Redirect::to('thong-tin-tai-khoan');
            }
            
        }else{
            return view('Staff.Login.Login');
        } 
    }
    public function PostLogin(Request $request){

        $user = User::where('phone', '=', $request->phone)->where('password', '=', md5($request->password))->first();

        if($user){
            if($user->role==2){
                if($user->active ==1 && $user->is_deleted ==0){
                    Auth::login($user,true);
                    return Redirect::to('/thong-tin-tai-khoan');
                }else{
                   return redirect()->back()->with('msg', 'Tài khoản của bạn bị tạm khóa'); 
                }
                
            }else{
                return redirect()->back()->with('msg', 'Sai tài khoản hoặc mật khẩu');
            }
        }else{
            return redirect()->back()->with('msg', 'Sai tài khoản hoặc mật khẩu'); 
        }  

 }


}
