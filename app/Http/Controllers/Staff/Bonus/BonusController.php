<?php

namespace App\Http\Controllers\Staff\Bonus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\User;

class BonusController extends Controller
{   
    public function ListBonus(Request $request){
        $GetBonus = DB::table('khen_thuong_ky_luat')
        ->leftJoin('users','users.id','khen_thuong_ky_luat.user_id')
        ->leftJoin('thong_tin_tai_khoan','thong_tin_tai_khoan.id','users.id')
        ->leftJoin('chuc_vu','chuc_vu.id','thong_tin_tai_khoan.chuc_vu')
        ->leftJoin('trinh_do','trinh_do.id','thong_tin_tai_khoan.trinh_do')
        ->select('thong_tin_tai_khoan.ho_va_ten','chuc_vu.ten_chuc_vu','khen_thuong_ky_luat.*')
        ->orderBy('khen_thuong_ky_luat.id', 'DESC')
        ->where('khen_thuong_ky_luat.loai',0)
        ->where('khen_thuong_ky_luat.xoa',0)
        ->where('khen_thuong_ky_luat.user_id',Auth::user()->id);

        if(isset($request->keyword)){
            $GetBonus=$GetBonus
            ->where('users.phone',$request->keyword)
            ->orWhere('thong_tin_tai_khoan.ho_va_ten',$request->keyword)
            ->where('khen_thuong_ky_luat.loai',0)
            ->where('khen_thuong_ky_luat.xoa',0)
            ->where('khen_thuong_ky_luat.user_id',Auth::user()->id)
            ->orWhere('thong_tin_tai_khoan.so_cmnd',$request->keyword)
            ->where('khen_thuong_ky_luat.loai',0)
            ->where('khen_thuong_ky_luat.xoa',0)
            ->where('khen_thuong_ky_luat.user_id',Auth::user()->id);
        }
        $GetBonus=$GetBonus->paginate(15);

        

        return view('Staff.Bonus.ListBonus',
            [
                'GetBonus'=>$GetBonus,

            ]
        );
    }
    

    

    
    
}
