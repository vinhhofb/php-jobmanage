<?php

namespace App\Http\Controllers\Staff\Discipline;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\User;

class DisciplineController extends Controller
{   
    public function ListDiscipline(Request $request){
        $GetDiscipline = DB::table('khen_thuong_ky_luat')
        ->leftJoin('users','users.id','khen_thuong_ky_luat.user_id')
        ->leftJoin('thong_tin_tai_khoan','thong_tin_tai_khoan.id','users.id')
        ->leftJoin('chuc_vu','chuc_vu.id','thong_tin_tai_khoan.chuc_vu')
        ->leftJoin('trinh_do','trinh_do.id','thong_tin_tai_khoan.trinh_do')
        ->select('thong_tin_tai_khoan.ho_va_ten','chuc_vu.ten_chuc_vu','khen_thuong_ky_luat.*')
        ->orderBy('khen_thuong_ky_luat.id', 'DESC')
        ->where('khen_thuong_ky_luat.loai',1)
        ->where('khen_thuong_ky_luat.xoa',0)
        ->where('khen_thuong_ky_luat.user_id',Auth::user()->id);
        

        if(isset($request->keyword)){
            $GetDiscipline=$GetDiscipline
            ->where('users.phone',$request->keyword)
            ->orWhere('thong_tin_tai_khoan.ho_va_ten',$request->keyword)
            ->where('khen_thuong_ky_luat.loai',1)
            ->where('khen_thuong_ky_luat.xoa',0)
            ->where('khen_thuong_ky_luat.user_id',Auth::user()->id)
            ->orWhere('thong_tin_tai_khoan.so_cmnd',$request->keyword)
            ->where('khen_thuong_ky_luat.loai',1)
            ->where('khen_thuong_ky_luat.xoa',0)
            ->where('khen_thuong_ky_luat.user_id',Auth::user()->id);
        }
        $GetDiscipline=$GetDiscipline->paginate(15);

        

        return view('Staff.Discipline.ListDiscipline',
            [
                'GetDiscipline'=>$GetDiscipline,

            ]
        );
    }
    

    

    public function EditDiscipline($id){
        $getDiscipline = DB::table('khen_thuong_ky_luat')->where('id',$id)->first();
        return view('Staff.Discipline.EditDiscipline',['getDiscipline'=>$getDiscipline,'id'=>$id]);
    }
    public function PostEditDiscipline($id,Request $request){
        $validate = $request->validate([
            'mo_ta' => 'required',
            'gia_tri' => 'required|integer',
        ]);
        $getDiscipline = DB::table('luong')->where('user_id',$id)->first();


        DB::table('khen_thuong_ky_luat')->where('id',$id)->update(
            [   
                'mo_ta'=>$request->mo_ta,
                'gia_tri'=>$request->gia_tri,
                'loai'=>1,
                'ngay_sua'=>time(),
                'nguoi_sua'=>Auth::user()->id,
            ]
        ); 
        
        
        return redirect('Staff/ky-luat');
    }

    public function AddDiscipline(){
        $getUsers = DB::table('users')->join('thong_tin_tai_khoan','thong_tin_tai_khoan.user_id','users.id')
        ->where('role',2)->get();

        return view('Staff.Discipline.AddDiscipline',['getUsers'=>$getUsers]);
    }
    public function PostAddDiscipline(Request $request){
        $validate = $request->validate([
            'user_id' => 'required|integer',
            'mo_ta' => 'required',
            'gia_tri' => 'required|integer',
        ]);
        
        DB::table('khen_thuong_ky_luat')->insert(
            [   
                'user_id'=>$request->user_id,
                'mo_ta'=>$request->mo_ta,
                'gia_tri'=>$request->gia_tri,
                'loai'=>1,
                'ngay_tao'=>time(),
                'nguoi_tao'=>Auth::user()->id,
            ]
        ); 
        
        return redirect('Staff/ky-luat');
    }

    public function DeleteDiscipline($id){

        DB::table('khen_thuong_ky_luat')->where('id',$id)->update(
            [   
                'xoa'=>1,
                'ngay_sua'=>time(),
                'nguoi_sua'=>Auth::user()->id,
            ]
        ); 
        return redirect('Staff/ky-luat');

    }
    
}
