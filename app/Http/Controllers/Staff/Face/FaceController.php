<?php

namespace App\Http\Controllers\Staff\Face;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Models\User;
use Carbon\Carbon;
use Session;

class FaceController extends Controller
{   
    public function FaceStaffDetail(){
        $getImages= DB::table('user_face')->where('user_id',Auth::user()->id)->get();
        return view('Staff.Face.FaceStaffDetail',
            [
                'getImages'=>$getImages,
            ]
        );
    }



    public function ConfirmFace(){
        Session::put('confirm_face', 'ok');
        return redirect(url('kenh-giao-hang/thong-tin-tai-khoan'));
    }
    public function RegisterFace(){
        $checkHaveFace = DB::table('user_face')->where('user_id',Auth::user()->id)
        ->orderBy('id','desc')
        ->first();
        if($checkHaveFace == null){
            return view('Staff.RegisterFace.Index');
        }else{
            return redirect('thong-tin-tai-khoan');
        }   
    }

    public function PostRegisterFace(Request $request){
        $getMax = DB::table('user_face')->where('user_id',Auth::user()->id)
        ->orderBy('id','desc')
        ->first();

        $getFullName = DB::table('thong_tin_tai_khoan')->where('user_id',Auth::user()->id)->first('ho_va_ten');
        if($getMax == null){
            $getMax=1;
        }else{
            $getMax = $getMax->order_by+1;
        }
        $image_64 = $request->image;
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
        $replace = substr($image_64, 0, strpos($image_64, ',')+1); 
        $image = str_replace($replace, '', $image_64); 
        $image = str_replace(' ', '+', $image); 
        $imageName = $getMax.'.jpg';
        Storage::put('public/face-data/'.$getFullName->ho_va_ten.'/'.$imageName, base64_decode($image));

        DB::table('user_face')->insert(['image'=>$imageName,'name'=>$getFullName->ho_va_ten,'order_by'=>$getMax,'created_at'=>time(),'user_id'=>Auth::user()->id]);
    }



    public function RecordFace(){
        Session::put('first_name', "ok");
        $getUsers =DB::table('users')
        ->leftJoin('user_face','user_face.user_id','users.id')
        ->where('users.role',2)
        ->where('users.is_deleted',0)
        ->select('user_face.name')
        ->where('user_face.name','!=',null)
        ->groupBy('user_face.name')
        ->get();
        
        return view('Staff.CheckFace.Index',['getUsers'=>$getUsers]);
    }

    public function PostRecordFace(Request $request){
        $getUser = DB::table('thong_tin_tai_khoan')->where('ho_va_ten',$request->name)->first('user_id');
        if(Session::get('first_name') != $request->name){
            $checkType = DB::table('user_track')->where('user_id',$getUser->user_id)->orderBy('id','desc')->first();

            if($checkType == null){
                $type=0;
            }else{
                if($checkType->type ==0){
                    $type=1;
                }else if($checkType->type ==1 ){
                    $type=0;
                }
            }
            
            DB::table('user_track')->insert(
                [
                    'user_id'=>$getUser->user_id,
                    'type' => $type,
                    'created_at'=>time()
                ]
            );
            Session::put('first_name', $request->name);
            if($type == 0){
                $time= $request->name." - Giờ vào ".Carbon::now('Asia/Ho_Chi_Minh');
            }else{
                $time= $request->name." - Giờ ra ".Carbon::now('Asia/Ho_Chi_Minh');
            }
            
            echo $time;
        }else{
            echo "Nhân viên ".$request->name." đã nhận diện thành công, xin mời người tiếp theo";
        }
        
    }
    
    
    
    
}
