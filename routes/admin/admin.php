<?php

use App\Http\Controllers\Admin\Account\AccountController;
use App\Http\Controllers\Admin\StaffManage\StaffManageController;
use App\Http\Controllers\Admin\UserManage\UserManageController;
use App\Http\Controllers\Admin\Department\DepartmentController;
use App\Http\Controllers\Admin\Position\PositionController;
use App\Http\Controllers\Admin\Level\LevelController;
use App\Http\Controllers\Admin\Specialize\SpecializeController;
use App\Http\Controllers\Admin\TypeStaff\TypeStaffController;
use App\Http\Controllers\Admin\Salary\SalaryController;
use App\Http\Controllers\Admin\Bonus\BonusController;
use App\Http\Controllers\Admin\Discipline\DisciplineController;
use App\Http\Controllers\Admin\Infomation\InfomationController;
use App\Http\Controllers\Admin\ChangePassword\ChangePasswordController;
use App\Http\Controllers\Admin\Work\WorkController;
use App\Http\Controllers\Admin\EmailCampaign\EmailCampaignController;
use App\Http\Controllers\Admin\Face\FaceController;
use Illuminate\Support\Facades\Route;



Route::prefix('admin')->group(function () {
    Route::group(['middleware' => 'checkadmin'], function () {
        Route::get('dang-xuat', [AccountController::class, 'Logout']);
        Route::prefix('quan-ly-nhan-vien')->group(function () {
            Route::get('/', [StaffManageController::class, 'ListStaff']);
            Route::get('xem-chi-tiet/{id}', [StaffManageController::class, 'StaffDetail']);
            Route::get('khoa-mo-nhan-vien/{id}', [StaffManageController::class, 'BlockUnBlockUser']);
            Route::get('tim-kiem', [StaffManageController::class, 'SearchUser']);

            Route::get('them-nhan-vien', [StaffManageController::class, 'AddStaff']);
            Route::post('them-nhan-vien', [StaffManageController::class, 'PostAddStaff']);
            Route::get('sua-nhan-vien/{id}', [StaffManageController::class, 'EditStaff']);
            Route::post('sua-nhan-vien/{id}', [StaffManageController::class, 'PostEditStaff']);
            Route::get('xoa/{id}', [StaffManageController::class, 'DeleteStaff']);
        });
        Route::prefix('quan-ly-tai-khoan')->group(function () {
            Route::get('/', [UserManageController::class, 'ListUser']);
            Route::get('xem-chi-tiet/{id}', [UserManageController::class, 'StaffDetail']);
            Route::get('khoa-mo-nhan-vien/{id}', [UserManageController::class, 'BlockUnBlockUser']);
            Route::get('tim-kiem', [UserManageController::class, 'SearchUser']);
        });

        Route::prefix('quan-ly-phong-ban')->group(function () {
            Route::get('/', [DepartmentController::class, 'ListDepartment']); 
            Route::get('/them', [DepartmentController::class, 'AddDepartment']); 
            Route::post('/them', [DepartmentController::class, 'PostAddDepartment']);   
            Route::get('/xoa/{id}', [DepartmentController::class, 'DeleteDepartment']); 
            Route::get('/sua/{id}', [DepartmentController::class, 'EditDepartment']); 
            Route::post('/sua/{id}', [DepartmentController::class, 'PostEditDepartment']);
            Route::get('/xem-nhan-vien/{id}', [DepartmentController::class, 'ListStaff']);          
        });

        Route::prefix('quan-ly-chuc-vu')->group(function () {
            Route::get('/', [PositionController::class, 'ListPosition']); 
            Route::get('/them', [PositionController::class, 'AddPosition']); 
            Route::post('/them', [PositionController::class, 'PostAddPosition']);   
            Route::get('/xoa/{id}', [PositionController::class, 'DeletePosition']); 
            Route::get('/sua/{id}', [PositionController::class, 'EditPosition']); 
            Route::post('/sua/{id}', [PositionController::class, 'PostEditPosition']); 
            Route::get('/xem-nhan-vien/{id}', [PositionController::class, 'ListStaff']);         
        });
        Route::prefix('quan-ly-trinh-do')->group(function () {
            Route::get('/', [LevelController::class, 'ListLevel']); 
            Route::get('/them', [LevelController::class, 'AddLevel']); 
            Route::post('/them', [LevelController::class, 'PostAddLevel']);   
            Route::get('/xoa/{id}', [LevelController::class, 'DeleteLevel']); 
            Route::get('/sua/{id}', [LevelController::class, 'EditLevel']); 
            Route::post('/sua/{id}', [LevelController::class, 'PostEditLevel']);  
            Route::get('/xem-nhan-vien/{id}', [LevelController::class, 'ListStaff']);           
        });
        Route::prefix('quan-ly-chuyen-mon')->group(function () {
            Route::get('/', [SpecializeController::class, 'ListSpecialize']); 
            Route::get('/them', [SpecializeController::class, 'AddSpecialize']); 
            Route::post('/them', [SpecializeController::class, 'PostAddSpecialize']);   
            Route::get('/xoa/{id}', [SpecializeController::class, 'DeleteSpecialize']); 
            Route::get('/sua/{id}', [SpecializeController::class, 'EditSpecialize']); 
            Route::post('/sua/{id}', [SpecializeController::class, 'PostEditSpecialize']);  
            Route::get('/xem-nhan-vien/{id}', [SpecializeController::class, 'ListStaff']);        
        });
        Route::prefix('quan-ly-loai-nhan-vien')->group(function () {
            Route::get('/', [TypeStaffController::class, 'ListTypeStaff']); 
            Route::get('/them', [TypeStaffController::class, 'AddTypeStaff']); 
            Route::post('/them', [TypeStaffController::class, 'PostAddTypeStaff']);   
            Route::get('/xoa/{id}', [TypeStaffController::class, 'DeleteTypeStaff']); 
            Route::get('/sua/{id}', [TypeStaffController::class, 'EditTypeStaff']); 
            Route::post('/sua/{id}', [TypeStaffController::class, 'PostEditTypeStaff']);
            Route::get('/xem-nhan-vien/{id}', [TypeStaffController::class, 'ListStaff']);          
        });

        Route::prefix('quan-ly-luong')->group(function () {
            Route::get('/', [SalaryController::class, 'ListSalary']); 
            Route::get('/sua/{id}', [SalaryController::class, 'EditSalary']); 
            Route::post('/sua/{id}', [SalaryController::class, 'PostEditSalary']);
            Route::get('bang-tinh-luong', [SalaryController::class, 'ListSalaryStaff']);  
            Route::get('bang-tinh-luong/xem-chi-tiet/{id}', [SalaryController::class, 'ListSalaryStaffDetail']);        
        });
        Route::prefix('khen-thuong')->group(function () {
            Route::get('/', [BonusController::class, 'ListBonus']);
            Route::get('/them', [BonusController::class, 'AddBonus']); 
            Route::post('/them', [BonusController::class, 'PostAddBonus']); 
            Route::get('/sua/{id}', [BonusController::class, 'EditBonus']); 
            Route::post('/sua/{id}', [BonusController::class, 'PostEditBonus']);  
            Route::get('/xoa/{id}', [BonusController::class, 'DeleteBonus']);        
        });
        Route::prefix('ky-luat')->group(function () {
            Route::get('/', [DisciplineController::class, 'ListDiscipline']);
            Route::get('/them', [DisciplineController::class, 'AddDiscipline']); 
            Route::post('/them', [DisciplineController::class, 'PostAddDiscipline']); 
            Route::get('/sua/{id}', [DisciplineController::class, 'EditDiscipline']); 
            Route::post('/sua/{id}', [DisciplineController::class, 'PostEditDiscipline']);  
            Route::get('/xoa/{id}', [DisciplineController::class, 'DeleteDiscipline']);        
        });
        Route::prefix('thong-tin-tai-khoan')->group(function () {
            Route::get('/', [InfomationController::class, 'Infomation']); 
            Route::post('/sua', [InfomationController::class, 'PostEditInfomation']);         
        });

        Route::prefix('doi-mat-khau')->group(function () {
            Route::get('/', [ChangePasswordController::class, 'ChangePassword']); 
            Route::post('/', [ChangePasswordController::class, 'PostChangePassword']);         
        });

        Route::prefix('quan-ly-cong-viec')->group(function () {
            Route::get('/', [WorkController::class, 'ListWork']);
            Route::get('/them', [WorkController::class, 'AddWork']); 
            Route::post('/them', [WorkController::class, 'PostAddWork']); 
            Route::get('/sua/{id}', [WorkController::class, 'EditWork']); 
            Route::post('/sua/{id}', [WorkController::class, 'PostEditWork']);  
            Route::get('/xoa/{id}', [WorkController::class, 'DeleteWork']);  
            Route::get('/chi-tiet-cong-viec/{id}', [WorkController::class, 'WorkDetail']);      
        });

        Route::prefix('chien-dich-email')->group(function () {
            Route::prefix('mau-email')->group(function () {
                Route::get('/', [EmailCampaignController::class, 'ListEmailTemplate']);
                Route::get('them', [EmailCampaignController::class, 'AddEmailTemplate']);
                Route::post('them', [EmailCampaignController::class, 'PostAddEmailTemplate']);
                Route::get('xoa/{id}', [EmailCampaignController::class, 'DeleteEmailTemplate']);
                Route::get('sua/{id}', [EmailCampaignController::class, 'EditEmailTemplate']);
                Route::post('sua/{id}', [EmailCampaignController::class, 'PostEditEmailTemplate']);
            });   
            Route::prefix('cau-hinh-email')->group(function () {
                Route::get('/', [EmailCampaignController::class, 'EmailConfig']);
                Route::post('/', [EmailCampaignController::class, 'PostEditEmailConfig']);

            }); 
            Route::prefix('gui-email')->group(function () {
                Route::get('them', [EmailCampaignController::class, 'AddEmailCampaign']);
                Route::post('them', [EmailCampaignController::class, 'PostAddEmailCampaign']);
            });   
        }); 
        Route::prefix('quan-ly-nhan-dien')->group(function () {
            Route::get('/', [FaceController::class, 'ListFaceStaff']);
            Route::get('/xem-du-lieu/{id}', [FaceController::class, 'FaceStaffDetail']);
            Route::get('/dang-ky-lai/{id}', [FaceController::class, 'ResetFaceStaff']);
        });
    });
Route::get('/dang-nhap', [AccountController::class, 'Login']);
Route::post('/dang-nhap', [AccountController::class, 'LoginPost']);
});



