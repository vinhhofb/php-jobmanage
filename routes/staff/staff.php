<?php

use App\Http\Controllers\Staff\Account\AccountController;
use App\Http\Controllers\Staff\Infomation\InfomationController;
use App\Http\Controllers\Staff\ChangePassword\ChangePasswordController;
use App\Http\Controllers\Staff\Face\FaceController;
use App\Http\Controllers\Staff\Work\WorkController;
use App\Http\Controllers\Staff\Bonus\BonusController;
use App\Http\Controllers\Staff\Discipline\DisciplineController;
use App\Http\Controllers\Staff\Salary\SalaryController;
use Illuminate\Support\Facades\Route;




Route::group(['middleware' => 'checkuser'], function () {
    Route::prefix('thong-tin-tai-khoan')->group(function () {
        Route::get('/', [InfomationController::class, 'Infomation']); 
        Route::post('/sua', [InfomationController::class, 'PostEditInfomation']);         
    });

    Route::prefix('doi-mat-khau')->group(function () {
        Route::get('/', [ChangePasswordController::class, 'ChangePassword']); 
        Route::post('/', [ChangePasswordController::class, 'PostChangePassword']);         
    });

    Route::prefix('quan-ly-cong-viec')->group(function () {
        Route::get('/', [WorkController::class, 'ListWork']); 
        Route::get('/chi-tiet-cong-viec/{id}', [WorkController::class, 'WorkDetail']);     
        Route::get('/cap-nhat-tien-do/{id}', [WorkController::class, 'UpdateProgress']); 
        Route::post('/cap-nhat-tien-do/{id}', [WorkController::class, 'PostUpdateProgress']); 
        Route::get('/hoan-thanh-cong-viec/{id}', [WorkController::class, 'FinishWork']); 
    });

    
    Route::prefix('quan-ly-nhan-dien')->group(function () {
        Route::get('/', [FaceController::class, 'FaceStaffDetail']);
    });

    Route::prefix('khen-thuong')->group(function () {
        Route::get('/', [BonusController::class, 'ListBonus']);;        
    });
    Route::prefix('ky-luat')->group(function () {
        Route::get('/', [DisciplineController::class, 'ListDiscipline']);       
    });
    Route::prefix('quan-ly-luong')->group(function () {
        Route::get('/', [SalaryController::class, 'ListSalary']); 

    });

    Route::get('/dang-ky-guong-mat', [FaceController::class, 'RegisterFace']);
    Route::post('/dang-ky-guong-mat', [FaceController::class, 'PostRegisterFace']);

    Route::get('/dang-xuat', [AccountController::class, 'Logout']);

    
});
Route::get('/cham-cong', [FaceController::class, 'RecordFace']);
Route::post('/nhan-dien-guong-mat', [FaceController::class, 'PostRecordFace']);



