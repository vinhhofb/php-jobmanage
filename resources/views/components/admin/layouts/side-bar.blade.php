


<nav class="sidebar sidebar-offcanvas" id="sidebar" style="position:fixed;">
  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#tongquan" aria-expanded="false" aria-controls="tongquan">
        <i class="icon-grid menu-icon"></i>
        <span class="menu-title">Tổng quan</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="tongquan">
        <ul class="nav flex-column sub-menu">
          {{-- <li class="nav-item"> <a class="nav-link" href="{{url('admin/thiet-lap')}}">Thống kê</a></li> --}}
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/quan-ly-nhan-vien')}}">Danh sách nhân viên</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/quan-ly-tai-khoan')}}">Danh sách tài khoản</a></li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#nhanvien" aria-expanded="false" aria-controls="nhanvien">
        <i class="icon-layout menu-icon"></i>
        <span class="menu-title">Nhân viên</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="nhanvien">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/quan-ly-phong-ban')}}">Phòng ban</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/quan-ly-chuc-vu')}}">Chức vụ</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/quan-ly-trinh-do')}}">Trình độ</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/quan-ly-chuyen-mon')}}">Chuyên môn</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/quan-ly-loai-nhan-vien')}}">Loại nhân viên</a></li>
        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#luong" aria-expanded="false" aria-controls="luong">
        <i class="icon-columns menu-icon"></i>
        <span class="menu-title">Quản lý lương</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="luong">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/quan-ly-luong')}}">Lương</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/quan-ly-luong/bang-tinh-luong')}}">Bảng tính lương</a></li>  
        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{url('admin/quan-ly-cong-viec')}}">
        <i class="icon-bar-graph menu-icon"></i>
        <span class="menu-title">Quản lý công việc</span>
      </a>
    </li>
    

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#khenthuong" aria-expanded="false" aria-controls="khenthuong">
        <i class="icon-grid-2 menu-icon"></i>
        <span class="menu-title">Khen thưởng-kỹ luật</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="khenthuong">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/khen-thuong')}}">Khen thưởng</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/ky-luat')}}">Kỷ luật</a></li>
          
        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#taikhoan" aria-expanded="false" aria-controls="taikhoan">
        <i class="icon-head menu-icon"></i>
        <span class="menu-title">Tài khoản</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="taikhoan">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/thong-tin-tai-khoan')}}">Thông tin tài khoản</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/doi-mat-khau')}}">Đổi mật khẩu</a></li>
        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{url('admin/quan-ly-nhan-dien')}}">
        <i class="icon-paper menu-icon"></i>
        <span class="menu-title">Quản lý nhận diện</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#email-campagn" aria-expanded="false" aria-controls="email-campagn">
        <i class="icon-ban menu-icon"></i>
        <span class="menu-title">Quản lý mail</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="email-campagn">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/chien-dich-email/mau-email')}}">Mẫu email</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/chien-dich-email/cau-hinh-email')}}">Cấu hình email</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('admin/chien-dich-email/gui-email/them')}}">Gửi email</a></li>
          
        </ul>
      </div>
    </li>


  </ul>
</nav>
