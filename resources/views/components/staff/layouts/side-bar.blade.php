<nav class="sidebar sidebar-offcanvas" id="sidebar" style="position:fixed;">
  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#tongquan" aria-expanded="false" aria-controls="tongquan">
        <i class="icon-grid menu-icon"></i>
        <span class="menu-title">Tài khoản</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="tongquan">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{url('thong-tin-tai-khoan')}}">Thông tin tài khoản</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('doi-mat-khau')}}">Đổi mật khẩu</a></li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{url('quan-ly-cong-viec')}}">
        <i class="icon-bar-graph menu-icon"></i>
        <span class="menu-title">Quản lý công việc</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{url('quan-ly-luong')}}">
        <i class="icon-bar-graph menu-icon"></i>
        <span class="menu-title">Quản lý lương</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#khenthuong" aria-expanded="false" aria-controls="khenthuong">
        <i class="icon-grid-2 menu-icon"></i>
        <span class="menu-title">Khen thưởng-kỹ luật</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="khenthuong">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{url('khen-thuong')}}">Khen thưởng</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('ky-luat')}}">Kỷ luật</a></li>
          
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{url('quan-ly-nhan-dien')}}">
        <i class="icon-paper menu-icon"></i>
        <span class="menu-title">Quản lý nhận diện</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{url('dang-ky-guong-mat')}}">
        <i class="icon-paper menu-icon"></i>
        <span class="menu-title">Đăng ký gương mặt</span>
      </a>
    </li>
  </ul>
</nav>
