@extends("Admin.Layouts.Master")
@section('Title', 'Danh sách nhân viên')
@section('Content')
<style type="text/css">
  @media only screen and (max-width: 900px) {
    td{
      white-space: nowrap;
    }
  }
</style>
<div class="container-scroller">
  <x-admin.layouts.header-dashboard/>
  <div class="container-fluid page-body-wrapper">
    <div class="theme-setting-wrapper">
    </div>
    <div class="side-bar-box" style="width: 250px;">
      <x-admin.layouts.side-bar/>
    </div>
    <div class="main-panel">
      <div class="content-wrapper p-3">
        <div class="row">
          <div class="col-md-12 grid-margin">
            <div class="row">
              <div class="col-12 col-xl-12 mb-4 mb-xl-0 p-0">
                <div>
                  <div>

                   <div class="bg-white">
                    <div class="col-lg-12 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body px-0">
                         <h5 class="card-title float-left mb-2 tx">Danh sách nhân viên</h5>
                         <div class="float-right"> 
                          <form method="get">    
                            <div class="form-group mb-3" style="display: flex"> 
                              <a href="{{url('admin/quan-ly-nhan-vien/them-nhan-vien')}}">
                                <div class="btn btn-success mr-2" style="width: 120px;">Thêm nhân viên</div>      
                              </a>                
                              <input type="text" class="form-control"  placeholder="Nhập CMND / Mã NV / Tên" name="keyword">
                              <button type="submit" class="btn bg text-white ml-2" style="width: 120px;">Tìm kiếm</button>
                            </div>
                          </form> 
                        </div>
                        <div style="clear: both;"></div>
                        <div class="table-responsive">
                          <table class="table table-hover table-striped">
                            <thead>
                              <th width="3%">Stt</th>
                              <th width="4%">Mã</th>
                              <th width="5%">Ảnh</th>
                              <th width="15%">Tên</th>
                              <th width="6%">Giới tính</th>
                              <th width="6%">Ngày sinh</th>
                              <th width="15%">Nơi sinh</th>
                              <th width="6%">Số CMND</th>
                              <th width="10%">Tình trạng</th>
                              <th width="15%">Thao tác</th>
                            </thead>
                            <tbody>
                             <p style="display: none">{{$idup = 1}}</p>
                             @foreach($GetListStaffs as $item)
                             <tr>
                              <td>{{$idup++}}</td>

                              <td>NV{{$item->user_id}}</td>
                              <td>
                                @if(isset($item->hinh_anh))
                                <img src="{{ asset('images/staff')."/".$item->hinh_anh}}">
                                @else
                                <img src="{{ asset('images/staff/default.png')}}">
                                @endif
                              </td>
                              <td>
                                {{$item->ho_va_ten}}
                              </td>
                              <td>
                                @if($item->gioi_tinh == 0)
                                Nam
                                @else
                                Nữ
                                @endif
                              </td>
                              <td>
                                {{\Carbon\Carbon::parse($item->ngay_sinh)->format('d/m/Y')}}
                              </td>
                              <td>
                                {{$item->noi_sinh}}
                              </td>
                              <td>
                                {{$item->so_cmnd}}
                              </td>
                              <td>
                                @if($item->trang_thai == 0)
                                Đang làm việc
                                @elseif($item->trang_thai == 1)
                                Nghỉ việc
                                @elseif($item->trang_thai == 2)
                                Tạm nghỉ
                                @endif
                              </td>
                              <td>
                                <a href="{{url('admin/quan-ly-nhan-vien/xem-chi-tiet')."/".$item->id}}">
                                  <button class="btn bg mr-2 text-white">Xem chi tiết</button>
                                </a>
                                <a href="{{url('admin/quan-ly-nhan-vien/sua-nhan-vien')."/".$item->id}}">
                                  <button class="btn btn-danger mr-2">Sửa</button>
                                </a>
                                                       
                              </td>
                            </tr>
                           

                          
                        @endforeach


                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="float-right pr-3">
           {{ $GetListStaffs->links('pagination::bootstrap-4') }}
         </div>
         <div style="clear: both"></div>
       </div>
     </div>

   </div>
 </div>
</div>
</div>
</div>
</div>   
</div>

@endsection











