@extends("Admin.Layouts.Master")
@section('Title', 'Chi tiết nhân viên')
@section('Content')

<style type="text/css">
  @media only screen and (max-width: 900px) {
    td{
      white-space: nowrap;
    }

  }
  p{
    font-size: 85% !important;
  }
</style>
<div class="container-scroller">
  <x-admin.layouts.header-dashboard/>
  <div class="container-fluid page-body-wrapper">
    <div class="theme-setting-wrapper">
    </div>
    <div class="side-bar-box" style="width: 250px;">
      <x-admin.layouts.side-bar/>
    </div>
    <div class="main-panel">
      <div class="content-wrapper p-3">
        <div class="row">
          <div class="col-md-12 grid-margin">
            <div class="row bg-white p-3">
              
              <div class="col-12 col-md-3 col-xl-3 mb-4 mb-xl-0 pr-4 pl-0">
                <div class="bg-white shadow-sm w-100">
                  @if(isset($GetStaffs->hinh_anh))
                  <img  src="{{ asset('images/staff')."/".$GetStaffs->hinh_anh}}" style="object-fit: cover;" width="100%" height="100%">
                  @else
                  <img  src="{{ asset('images/staff/default.png')}}" style="object-fit: cover;" width="100%" height="100%">
                  @endif
                </div>
              </div>
              <div class="col-12 col-md-4 p-2">
                <h5 class="font-weight-bold tx">Thông tin chi tiết</h5>
                <p class=" mb-2">Họ và tên: {{$GetStaffs->ho_va_ten}}</p>
                <p class=" mb-2">Biệt danh: {{$GetStaffs->biet_danh}}</p>
                <p class=" mb-2">Giới tính: @if($GetStaffs->gioi_tinh == 0)Nam @else Nữ@endif</p>
                <p class=" mb-2">Ngày sinh: {{\Carbon\Carbon::parse($GetStaffs->ngay_sinh)->format('d/m/Y')}}</p>
                <p class=" mb-2">Nơi sinh: {{$GetStaffs->noi_sinh}}</p>
                <p class=" mb-2">Số điện thoại: <a href="tel:{{$GetStaffs->so_dien_thoai}}">{{$GetStaffs->so_dien_thoai}}</a></p>
                 <p class=" mb-2">Email: <a href="mailto:{{$GetStaffs->email}}">{{$GetStaffs->email}}</a></p>
                <p class=" mb-2">Tình trạng hôn nhân: @if($GetStaffs->tinh_trang_hon_nhan == 0) Chưa kết hôn @else Đã kết hôn @endif</p>
                <p class=" mb-2">Số CMND: {{$GetStaffs->so_cmnd}}</p>
                <p class=" mb-2">Ngày cấp: {{\Carbon\Carbon::parse($GetStaffs->ngay_cap)->format('d/m/Y')}}</p>
                <p class=" mb-2">Nguyên quán: {{$GetStaffs->nguyen_quan}}</p>
                <p class=" mb-2">Quốc tịch: {{$GetStaffs->noi_cap}}</p>
                <p class=" mb-2">Nơi cấp: {{$GetStaffs->quoc_tich}}</p>
                <p class=" mb-2">Dân tộc: {{$GetStaffs->dan_toc}}</p>
                <p class=" mb-2">Tôn giáo: {{$GetStaffs->ton_giao}}</p>  
              </div>
              <div class="col-12 col-md-5 p-2">
                 <p class=" mb-2">Hộ khẩu: {{$GetStaffs->ho_khau}}</p>
                <p class=" mb-2">Tạm trú: {{$GetStaffs->tam_tru}}</p>
                <p class=" mb-2">Loại nhân viên: {{$GetStaffs->loai_nhan_vien}}</p>
                <p class=" mb-2">Trình độ: {{$GetStaffs->trinh_do}}</p>
                <p class=" mb-2">Chuyên môn: {{$GetStaffs->chuyen_mon}}</p>
                <p class=" mb-2">Phòng ban: {{$GetStaffs->phong_ban}}</p>
                <p class=" mb-2">Chức vụ: {{$GetStaffs->chuc_vu  }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>   
  </div>

  @endsection











