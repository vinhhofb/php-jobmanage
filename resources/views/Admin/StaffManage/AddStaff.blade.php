@extends("Admin.Layouts.Master")
@section('Title', 'Thêm nhân viên')
@section('Content')
<div class="container-scroller">
  <x-admin.layouts.header-dashboard/>
  <div class="container-fluid page-body-wrapper">
    <div class="theme-setting-wrapper">
    </div>
    <div class="side-bar-box" style="width: 250px;">
      <x-admin.layouts.side-bar/>
    </div>
    <div class="main-panel">
      <div class="content-wrapper px-0 py-3">
        <div class="row m-0">
          <div class="col-md-12 grid-margin p-0">
            <div class="row m-0">
              <div class="col-12 col-xl-12 mb-4 mb-xl-0 p-0">
                <div>
                  <div class="bg-white p-2">
                   <h5 class="card-title mb-4 font-weight-bold ml-2 mt-2 tx">Thêm nhân viên</h5>
                   <form id="form-add-steff" method="post" action="{{url('admin/quan-ly-nhan-vien/them-nhan-vien')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row m-0">
                      <div class="col-12 px-2">
                        <p class="font-weight-bold">| Thông tin cá nhân</p>
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Họ và tên</label>
                        <input type="text" name="ho_va_ten" class="form-control mr-2"  autocomplete="off">
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Biệt danh</label>
                        <input type="text" name="biet_danh" class="form-control mr-2"  autocomplete="off" >
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Hình ảnh</label>
                        <input type="file" name="hinh_anh" class="form-control mr-2" accept="image/png, image/gif, image/jpeg">
                      </div>

                      <div class="col-12 px-2 mt-3">
                        <p class="font-weight-bold">| Thông tin liên hệ & đăng nhập</p>
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Số điện thoại</label>
                        <input type="number" name="so_dien_thoai" class="form-control mr-2" autocomplete="off" >
                      </div>    
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Email</label>
                        <input type="text" name="email" class="form-control mr-2" autocomplete="off" >
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Mật khẩu</label>
                        <input type="password" name="password" class="form-control mr-2" autocomplete="off" >
                      </div>
                      <div class="col-12 px-2 mt-3">
                        <p class="font-weight-bold">| Sơ yếu lý lịch</p>
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Giới tính</label>
                        <select name="gioi_tinh" class="form-control" id="exampleFormControlSelect1">
                          <option value="0">Nam</option>
                          <option value="1">Nữ</option> 
                        </select>
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Ngày sinh</label>
                        <input type="date" name="ngay_sinh" class="form-control mr-2" autocomplete="off" >
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Nơi sinh</label>
                        <input type="text" name="noi_sinh" class="form-control mr-2" autocomplete="off" >
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Tình trạng hôn nhân</label>
                        <select name="tinh_trang_hon_nhan" class="form-control" id="exampleFormControlSelect1">
                          <option value="0">Đã kết hôn</option>
                          <option value="1">Chưa kết hôn</option> 
                        </select>
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Số CMND</label>
                        <input type="text" name="so_cmnd" class="form-control mr-2" autocomplete="off" >
                      </div> 
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Ngày cấp</label>
                        <input type="date" name="ngay_cap" class="form-control mr-2" autocomplete="off" >
                      </div> 
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Nơi cấp</label>
                        <input type="text" name="noi_cap" class="form-control mr-2" autocomplete="off" >
                      </div> 
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Nguyên quán</label>
                        <input type="text" name="nguyen_quan" class="form-control mr-2" autocomplete="off" >
                      </div> 
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Quốc tịch</label>
                        <input type="text" name="quoc_tich" class="form-control mr-2" autocomplete="off" >
                      </div>  
                      
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Dân tộc</label>
                        <input type="text" name="dan_toc" class="form-control mr-2" autocomplete="off" >
                      </div>  
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Tôn giáo</label>
                        <input type="text" name="ton_giao" class="form-control mr-2" autocomplete="off" >
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Hộ khẩu</label>
                        <input type="text" name="ho_khau" class="form-control mr-2" autocomplete="off" >
                      </div> 
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Tạm trú</label>
                        <input type="text" name="tam_tru" class="form-control mr-2" autocomplete="off" >
                      </div> 
                      <div class="col-12 px-2 mt-3">
                        <p class="font-weight-bold">| Thông tin công việc</p>
                      </div>  
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Loại nhân viên</label>
                        <select name="loai_nhan_vien" class="form-control" id="exampleFormControlSelect1">
                          @foreach($loai_nhan_vien as $item)
                          <option value="{{$item->id}}">{{$item->ten_loai}}</option>
                          @endforeach

                        </select>
                      </div> 
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Trình độ</label>
                        <select name="trinh_do" class="form-control" id="exampleFormControlSelect1">
                          @foreach($trinh_do as $item)
                          <option value="{{$item->id}}">{{$item->ten_trinh_do}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Chuyên môn</label>
                        <select name="chuyen_mon" class="form-control" id="exampleFormControlSelect1">
                          @foreach($chuyen_mon as $item)
                          <option value="{{$item->id}}">{{$item->ten_chuyen_mon}}</option>
                          @endforeach
                        </select>
                      </div> 
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Phòng ban</label>
                        <select name="phong_ban" class="form-control" id="exampleFormControlSelect1">
                          @foreach($phong_ban as $item)
                          <option value="{{$item->id}}">{{$item->ten_phong_ban}}</option>
                          @endforeach

                        </select>
                      </div>   
                      <div class="col-12 col-sm-6 col-md-4 p-0 px-2 mb-2">
                        <label class="fz85">Chức vụ</label>
                        <select name="chuc_vu" class="form-control" id="exampleFormControlSelect1">
                          @foreach($chuc_vu as $item)
                          <option value="{{$item->id}}">{{$item->ten_chuc_vu}}</option>
                          @endforeach

                        </select>
                      </div> 
                      
                      <div class="col-12 p-0 pr-2 mb-2 text-center mt-3">
                        <button class="btn bg text-white">Thêm nhân viên</button>
                      </div>
                    </div>
                    @if($errors->any())
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                    @endif
                    @if (\Session::has('msg'))
                    <p class="text-danger mt-2 text-center mb-0 fz-95">{!! \Session::get('msg') !!}</p>
                    @endif
                  </form>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>   
</div>
<script src="{{ asset('index/js/jquery-3.6.0.js') }}"></script>
<script src="{{ asset('index/js/validate/jquery.validate.min.js') }}" ></script>
<script src="{{ asset('index/js/validate/validate.js') }}"></script>
@endsection








