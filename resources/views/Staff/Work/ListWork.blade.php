@extends("Staff.Layouts.Master")
@section('Title', 'Danh sách công việc')
@section('Content')
<style type="text/css">
  @media only screen and (max-width: 900px) {
    td{
      white-space: nowrap;
    }
  }
</style>
<div class="container-scroller">
  <x-staff.layouts.header-dashboard/>
  <div class="container-fluid page-body-wrapper">
    <div class="theme-setting-wrapper">
    </div>
    <div class="side-bar-box" style="width: 250px;">
      <x-staff.layouts.side-bar/>
    </div>
    <div class="main-panel">
      <div class="content-wrapper p-3">
        <div class="row">
          <div class="col-md-12 grid-margin">
            <div class="row">
              <div class="col-12 col-xl-12 mb-4 mb-xl-0 p-0">
                <div>
                  <div>

                   <div class="bg-white">
                    <div class="col-lg-12 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body px-0">
                         <h5 class="card-title float-left mb-2 tx">Danh sách công việc</h5>
                         
                         <div class="table-responsive">
                          <table class="table table-hover table-striped">
                            <thead>
                              <th width="3%">Stt</th>
                              <th width="4%">Mã</th>
                              
                              <th width="18%">Công việc</th>
                              <th width="18%">Ngày</th>
                              <th width="10%">Trạng thái</th>
                              <th width="10%">Ngày tạo</th>
                              <th width="24%">Thao tác</th>
                            </thead>
                            <tbody>
                             <p style="display: none">{{$idup = 1}}</p>
                             @foreach($GetWork as $item)
                             <tr>
                              <td>{{$idup++}}</td>
                              <td>KT{{$item->id}}</td>
                              
                              <td>
                                {{$item->ten_cong_viec}}
                              </td>
                              <td>
                               Từ ngày {{\Carbon\Carbon::parse($item->tu_ngay)->format('d/m/Y')}} Đến ngày {{\Carbon\Carbon::parse($item->den_ngay)->format('d/m/Y')}}
                             </td>
                             <td>
                               @if($item->trang_thai == 0)
                               <span class="text-warning">Đang thực hiện</span>
                               @else
                               <span class="text-success">Đã hoàn tất</span>
                               @endif
                             </td>
                             <td>
                               {{\Carbon\Carbon::parse($item->ngay_tao)->format('d/m/Y')}}
                             </td>
                             <td>
                              <a href="{{url('quan-ly-cong-viec/chi-tiet-cong-viec')."/".$item->id}}">
                                <button class="btn btn-success mr-2 text-white">Xem</button>
                              </a>
                              @if($item->trang_thai == 0)
                              <a href="{{url('quan-ly-cong-viec/cap-nhat-tien-do')."/".$item->id}}">
                                <button class="btn btn-success mr-2 text-white">Cập nhật tiến độ</button>
                              </a>                       
                              @endif         
                            </td>
                          </tr>

                          <div class="modal fade mt-5" id="exampleModalUnLock{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Xóa loại nhân viên</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                 <p>Bạn đồng ý xóa công việc?</p>
                               </div>
                               <div class="p-2">
                                 <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Hủy</button>
                                 <a  href="{{url('admin/quan-ly-cong-viec/xoa')."/".$item->id}}">
                                  <button type="button" class="btn btn-danger float-right mr-2">
                                    Xóa                
                                  </button>
                                </a>


                                <div style="clear: both"></div>
                              </div>
                            </div>
                          </div>
                        </div>

                        @endforeach


                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="float-right pr-3">
           {{ $GetWork->links('pagination::bootstrap-4') }}
         </div>
         <div style="clear: both"></div>
       </div>
     </div>

   </div>
 </div>
</div>
</div>
</div>
</div>   
</div>

@endsection











